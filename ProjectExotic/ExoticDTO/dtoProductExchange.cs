﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoProductExchange
    {
        string _ExportBillID;

        public string ExportBillID
        {
            get { return _ExportBillID; }
            set { _ExportBillID = value; }
        }
        string _OldProductID;

        public string OldProductID
        {
            get { return _OldProductID; }
            set { _OldProductID = value; }
        }
        string _NewProductID;

        public string NewProductID
        {
            get { return _NewProductID; }
            set { _NewProductID = value; }
        }
        int _Quantity;

        public int Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        DateTime _Time;

        public DateTime Time
        {
            get { return _Time; }
            set { _Time = value; }
        }
    }
}
