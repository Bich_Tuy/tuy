﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoSizes
    {
        string _SizeID;

        public string SizeID
        {
            get { return _SizeID; }
            set { _SizeID = value; }
        }
        string _SizeName;

        public string SizeName
        {
            get { return _SizeName; }
            set { _SizeName = value; }
        }
        string _Categories;

        public string Categories
        {
            get { return _Categories; }
            set { _Categories = value; }
        }
    }
}
