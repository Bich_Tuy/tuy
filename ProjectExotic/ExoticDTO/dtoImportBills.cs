﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoImportBills
    {
        string _ImportBillID;

        public string ImportBillID
        {
            get { return _ImportBillID; }
            set { _ImportBillID = value; }
        }

        string _SupplierID;

        public string SupplierID
        {
            get { return _SupplierID; }
            set { _SupplierID = value; }
        }

        string _EmployeeID;

        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }

        DateTime _Time;

        public DateTime Time
        {
            get { return _Time; }
            set { _Time = value; }
        }

        bool _IsInDebt;

        public bool IsInDebt
        {
            get { return _IsInDebt; }
            set { _IsInDebt = value; }
        }
        DateTime _DebtDeadline;

        public DateTime DebtDeadline
        {
            get { return _DebtDeadline; }
            set { _DebtDeadline = value; }
        }
    }
}
