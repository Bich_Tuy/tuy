﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoEmployees
    {
        string _EmployeeID;

        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        string _EmployTypeID;

        public string EmployTypeID
        {
            get { return _EmployTypeID; }
            set { _EmployTypeID = value; }
        }
        string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        DateTime _Birthday;

        public DateTime Birthday
        {
            get { return _Birthday; }
            set { _Birthday = value; }
        }
        DateTime _Hireday;

        public DateTime Hireday
        {
            get { return _Hireday; }
            set { _Hireday = value; }
        }
        bool _Gender;

        public bool Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        string _IDNumber;

        public string IDNumber
        {
            get { return _IDNumber; }
            set { _IDNumber = value; }
        }
        string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        string _Phone;

        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        byte[] _IdentifyImage;

        public byte[] IdentifyImage
        {
            get { return _IdentifyImage; }
            set { _IdentifyImage = value; }
        }
        string _Username;

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }
        string _Password;

        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
    }
}
