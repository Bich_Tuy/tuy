﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoImportDebtPayments
    {
        int _ID;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        string _ImportBillID;

        public string ImportBillID
        {
            get { return _ImportBillID; }
            set { _ImportBillID = value; }
        }

        decimal _Money;

        public decimal Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        DateTime _Time;

        public DateTime Time
        {
            get { return _Time; }
            set { _Time = value; }
        }
    }
}
