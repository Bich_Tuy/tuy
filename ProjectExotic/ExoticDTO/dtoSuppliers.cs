﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoSuppliers
    {
        string _SupplierID;

        public string SupplierID
        {
            get { return _SupplierID; }
            set { _SupplierID = value; }
        }

        string _SupplierName;

        public string SupplierName
        {
            get { return _SupplierName; }
            set { _SupplierName = value; }
        }

        string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        string _Country;

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        string _Phone;

        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        string _Fax;

        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }

        string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
    }
}
