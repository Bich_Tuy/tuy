﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoExportBillDetails
    {
        string _ExportBillID;

        public string ExportBillID
        {
            get { return _ExportBillID; }
            set { _ExportBillID = value; }
        }
        string _ProductID;

        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        int _Quantity;

        public int Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        decimal _UnitPrice;

        public decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }
        decimal _Totality;

        public decimal Totality
        {
            get { return _Totality; }
            set { _Totality = value; }
        }
        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
    }
}
