﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoEmployeeTypes
    {
        string _EmployeeTypeID;

        public string EmployeeTypeID
        {
            get { return _EmployeeTypeID; }
            set { _EmployeeTypeID = value; }
        }
        string _EmployeeTypeName;

        public string EmployeeTypeName
        {
            get { return _EmployeeTypeName; }
            set { _EmployeeTypeName = value; }
        }
        float _SalaryCoefficient;

        public float SalaryCoefficient
        {
            get { return _SalaryCoefficient; }
            set { _SalaryCoefficient = value; }
        }
    }
}
