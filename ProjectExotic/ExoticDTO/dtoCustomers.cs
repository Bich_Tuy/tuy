﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoCustomers
    {
        string _CustomerID;

        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }
        string _CustomerTypeID;

        public string CustomerTypeID
        {
            get { return _CustomerTypeID; }
            set { _CustomerTypeID = value; }
        }
        string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        bool _Gender;

        public bool Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        string _IDNumber;

        public string IDNumber
        {
            get { return _IDNumber; }
            set { _IDNumber = value; }
        }
        string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        string _Phone;

        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        string _Phone1;

        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        DateTime _RegisterDate;

        public DateTime RegisterDate
        {
            get { return _RegisterDate; }
            set { _RegisterDate = value; }
        }
    }
}
