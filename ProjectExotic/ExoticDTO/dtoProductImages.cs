﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoProductImages
    {
        int _ProductImageID;

        public int ProductImageID
        {
            get { return _ProductImageID; }
            set { _ProductImageID = value; }
        }
        string _ProductInfoID;

        public string ProductInfoID
        {
            get { return _ProductInfoID; }
            set { _ProductInfoID = value; }
        }
        byte[] _Image;

        public byte[] Image
        {
            get { return _Image; }
            set { _Image = value; }
        }
        string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
    }
}
