﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoShipments
    {
        int _ShipmentID;

        public int ShipmentID
        {
            get { return _ShipmentID; }
            set { _ShipmentID = value; }
        }
        
        string _ExportBillID;

        public string ExportBillID
        {
            get { return _ExportBillID; }
            set { _ExportBillID = value; }
        }
        string _CustomerID;

        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }
        string _EmployeeID;

        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        string _Adress;

        public string Adress
        {
            get { return _Adress; }
            set { _Adress = value; }
        }
        string _Phone;

        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        string _Phone1;

        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        decimal _ShippingPrices;

        public decimal ShippingPrices
        {
            get { return _ShippingPrices; }
            set { _ShippingPrices = value; }
        }
        DateTime _Time;

        public DateTime Time
        {
            get { return _Time; }
            set { _Time = value; }
        }
    }
}
