﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoCustomerTypes
    {
        string _CustomerTypeID;

        public string CustomerTypeID
        {
            get { return _CustomerTypeID; }
            set { _CustomerTypeID = value; }
        }
        string _CustomerTypeName;

        public string CustomerTypeName
        {
            get { return _CustomerTypeName; }
            set { _CustomerTypeName = value; }
        }
        float _DiscountRatio;

        public float DiscountRatio
        {
            get { return _DiscountRatio; }
            set { _DiscountRatio = value; }
        }
    }
}
