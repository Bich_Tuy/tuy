﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoProducts
    {
        string _ProductID;

        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        string _ProductInfoID;

        public string ProductInfoID
        {
            get { return _ProductInfoID; }
            set { _ProductInfoID = value; }
        }
        string _SizeID;

        public string SizeID
        {
            get { return _SizeID; }
            set { _SizeID = value; }
        }
        int _UnitsInStock;

        public int UnitsInStock
        {
            get { return _UnitsInStock; }
            set { _UnitsInStock = value; }
        }
        int _QuantitySold;

        public int QuantitySold
        {
            get { return _QuantitySold; }
            set { _QuantitySold = value; }
        }
        bool _State;

        public bool State
        {
            get { return _State; }
            set { _State = value; }
        }
    }
}
