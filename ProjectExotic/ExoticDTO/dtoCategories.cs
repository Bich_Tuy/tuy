﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoCategories
    {
        string _CategoryID;

        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        string _CategoryName;

        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
    }
}
