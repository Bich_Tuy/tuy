﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoProductInfos
    {
        string _ProductInfoID;

        public string ProductInfoID
        {
            get { return _ProductInfoID; }
            set { _ProductInfoID = value; }
        }
        string _CategoryID;

        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        string _BrandID;

        public string BrandID
        {
            get { return _BrandID; }
            set { _BrandID = value; }
        }
        string _ProductInfoName;

        public string ProductInfoName
        {
            get { return _ProductInfoName; }
            set { _ProductInfoName = value; }
        }
        string _CalculationUnit;

        public string CalculationUnit
        {
            get { return _CalculationUnit; }
            set { _CalculationUnit = value; }
        }
        string _Color;

        public string Color
        {
            get { return _Color; }
            set { _Color = value; }
        }
        string _Material;

        public string Material
        {
            get { return _Material; }
            set { _Material = value; }
        }
        decimal _WholesalePrice;

        public decimal WholesalePrice
        {
            get { return _WholesalePrice; }
            set { _WholesalePrice = value; }
        }
        decimal _RetailPrice;

        public decimal RetailPrice
        {
            get { return _RetailPrice; }
            set { _RetailPrice = value; }
        }
        string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
    }
}
