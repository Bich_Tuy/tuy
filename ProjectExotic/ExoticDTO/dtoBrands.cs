﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoticDTO
{
    public class dtoBrands
    {
        string _BrandID;

        public string BrandID
        {
            get { return _BrandID; }
            set { _BrandID = value; }
        }
        string _BrandName;

        public string BrandName
        {
            get { return _BrandName; }
            set { _BrandName = value; }
        }
        string _Country;

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }
        string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
    }
}
