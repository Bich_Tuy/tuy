﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDAL;
using ExoticDTO;

namespace ExoticBLL
{
    public class bllShipments
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        public IQueryable getTable()
        {
            ShopCommerceDataContext dbs = new ShopCommerceDataContext();

            var data = (from n in dbs.Shipments
                        select new
                        {
                            ShipmentID=n.ShipmentID,
                            CutomerID=n.CustomerID,
                            EmployeeID=n.EmployeeID,
                            ExportBillID=n.ExportBillID,
                            CustomerName=(n.Customer.FirstName+" "+n.Customer.LastName).Trim(),
                            EmployeeName=(n.Employee.FirstName+" "+n.Employee.LastName).Trim(),
                            Address=n.Address,
                            Phone=n.Phone,Phone1=n.Phone1,
                            ShippingPrices=n.ShippingPrices,
                            Time=n.Time
                        });
            return data;
        }
        //insert data
        public bool insertData(dtoShipments dto, ref string err)
        {
            bool sucess;
            // tao doi tuong rong
            Shipment dt = new Shipment();
            //nap du lieu cho doi tuong
            dt.Address = dto.Adress;
            dt.CustomerID = dto.CustomerID;
            dt.EmployeeID = dto.EmployeeID;
            dt.ExportBillID = dto.ExportBillID;
            dt.Phone = dto.Phone;
            dt.Phone1 = dto.Phone1;
            dt.ShipmentID = dto.ShipmentID;
            dt.ShippingPrices = dto.ShippingPrices;
            dt.Time = dto.Time;

            // nap doi tuong vao co so du lieu
            try
            {
                dbs.Shipments.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                sucess = true;
            }
            //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
            catch (Exception error)
            {
                err = error.Message;
                sucess = false;
            }
            return sucess;
        }
        //update data
        public bool updateData(dtoShipments dto, ref string err)
        {
            bool sucess;
            Shipment dt = (from n in dbs.Shipments
                           where n.ShipmentID == dto.ShipmentID
                           select n).Single();

            //nap du lieu cho doi tuong
            dt.Address = dto.Adress;
            dt.CustomerID = dto.CustomerID;
            dt.EmployeeID = dto.EmployeeID;
            dt.ExportBillID = dto.ExportBillID;
            dt.Phone = dto.Phone;
            dt.Phone1 = dto.Phone1;
            dt.ShipmentID = dto.ShipmentID;
            dt.ShippingPrices = dto.ShippingPrices;
            dt.Time = dto.Time;

            // nap doi tuong vao co so du lieu
            try
            {
                dbs.SubmitChanges();
                sucess = true;
            }
            //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
            catch (Exception error)
            {
                err = error.Message;
                sucess = false;
            }
            return sucess;
        }
        // delete data
        public bool deleteData(dtoShipments dto, ref string err)
        {
            bool sucess;
            Shipment dt = (from n in dbs.Shipments
                           where n.ShipmentID == dto.ShipmentID
                           select n).Single();

            try
            {
                dbs.Shipments.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                sucess = true;
            }
            //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
            catch (Exception error)
            {
                err = error.Message;
                sucess = false;
            }
            return sucess;
        }
    }
}

