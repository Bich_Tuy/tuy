﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;

namespace ExoticBLL
{
    public class bllExportBillDetails
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        public IQueryable getTable()
        {
            var data = (from n in dbs.ExportBills select n);
            return data;
        }
        //insert data

        public bool insertData(dtoExportBillDetails dto, ref string err)
        {
            bool success;
            //tao doi tuong rong
            ExportBillDetail dt = new ExportBillDetail();
            dt.ExportBillID = dto.ExportBillID;
            dt.ProductID = dto.ProductID;
            dt.Quantity = dto.Quantity;
            dt.UnitPrice = dto.UnitPrice;
            dt.Totality = dto.Totality;
            dt.Description = dto.Description;

            try
            {
                dbs.ExportBillDetails.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }

        public bool updateData(dtoExportBillDetails dto, ref string err)
        {
            bool success;
            ExportBillDetail dt = (from n in dbs.ExportBillDetails
                             where n.ExportBillID == dto.ExportBillID
                             select n).Single();
            //nap du lieu cho doi tuong
            dt.ExportBillID = dto.ExportBillID;
            dt.ProductID = dto.ProductID;
            dt.Quantity = dto.Quantity;
            dt.UnitPrice = dto.UnitPrice;
            dt.Totality = dto.Totality;
            dt.Description = dto.Description;
            try
            {
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }

        //dalete data
        public bool deleteData(dtoExportBillDetails dto, ref string err)
        {
            bool success;
            ExportBillDetail dt = (from n in dbs.ExportBillDetails
                             where n.ExportBillID == dto.ExportBillID
                             select n).Single();
            //xoa du lieu

            try
            {
                dbs.ExportBillDetails.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        
    }
}
