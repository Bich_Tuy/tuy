﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDAL;
using ExoticDTO;

namespace ExoticBLL
{
        public class bllEmployees
        {
            ShopCommerceDataContext dbs = new ShopCommerceDataContext();
            //Get Table
            public IQueryable getTable()
            {
                var data = (from n in dbs.Employees
                            select new
                            {
                                EmployeeID = n.EmployeeID,
                                EmployeeTypeName = n.EmployeeType.EmployeeTypeName,
                                FirstName = n.FirstName,
                                LastName = n.LastName,
                                Birthday = n.Birthday,
                                Hireday = n.Hireday,
                                Gender = n.Gender,
                                Address = n.Address,
                                IdentifyImage = n.IdentifyImage,
                                Phone=n.Phone
                            });
                return data;
            }
            //insert data
            public bool insertData(dtoEmployees dto, ref string err)
            {
                bool sucess;
                // tao doi tuong rong
                Employee dt = new Employee();
                //nap du lieu cho doi tuong
                dt.Address = dto.Address;
                dt.Birthday = dto.Birthday;
                dt.EmployeeID = dto.EmployeeID;
                dt.EmployeeTypeID = dto.EmployTypeID;
                dt.FirstName = dto.FirstName;
                dt.Gender = dto.Gender;
                dt.Hireday = dto.Hireday;
                dt.IdentifyImage = dto.IdentifyImage;
                dt.LastName = dto.LastName;
                dt.Password = dto.Password;
                dt.Phone = dto.Phone;
                dt.Username = dto.Username;
               

                try
                {
                    dbs.Employees.InsertOnSubmit(dt);
                    dbs.SubmitChanges();
                    sucess = true;
                }
                //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
                catch (Exception error)
                {
                    err = error.Message;
                    sucess = false;
                }
            return sucess;
        }
            //update data
            public bool updateData(dtoEmployees dto, ref string err)
            {
                bool sucess;
                Employee dt = (from n in dbs.Employees
                               where n.EmployeeID == dto.EmployeeID
                               select n).Single();
    
                //nap du lieu cho doi tuong
                dt.Address = dto.Address;
                dt.Birthday = dto.Birthday;
                dt.EmployeeID = dto.EmployeeID;
                dt.EmployeeTypeID = dt.EmployeeTypeID;
                dt.FirstName = dt.FirstName;
                dt.Gender = dt.Gender;
                dt.Hireday = dt.Hireday;
                dt.IdentifyImage = dt.IdentifyImage;
                dt.LastName = dt.LastName;
                dt.Password = dt.Password;
                dt.Phone = dt.Phone;
                dt.Username = dt.Username;


                // nap doi tuong vao co so du lieu
                try
                {
                    dbs.SubmitChanges();
                    sucess = true;
                }
                //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
                catch (Exception error)
                {
                    err = error.Message;
                    sucess = false;
                }
                return sucess;
            }
            // delete data
            public bool deleteData(dtoEmployees dto, ref string err)
            {
                bool sucess;
                Employee dt = (from n in dbs.Employees
                               where n.EmployeeID == dto.EmployeeID
                               select n).Single();

                try
                {
                    dbs.Employees.DeleteOnSubmit(dt);
                    dbs.SubmitChanges();
                    sucess = true;
                }
                //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
                catch (Exception error)
                {
                    err = error.Message;
                    sucess = false;
                }
                return sucess;
            }
        }
    }

