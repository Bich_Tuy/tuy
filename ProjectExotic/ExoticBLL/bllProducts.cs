﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;

namespace ExoticBLL
{
    public class bllProducts
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get table
        public IQueryable getTable()
        {
            var data = (from n in dbs.Products
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoProducts dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            Product dt = new Product();
            //Nạp dữ liệu cho đối tượng
            dt.ProductID = dto.ProductID;
            dt.ProductInfoID = dto.ProductInfoID;
            dt.QuantitySold = dto.QuantitySold;
            dt.SizeID = dto.SizeID;
            dt.UnitsInStock = dto.UnitsInStock;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.Products.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //UpdateData
        public bool updateData(dtoProducts dto, ref string err)
        {
            bool success;
            Product dt = (from n in dbs.Products
                              where n.ProductID == dto.ProductID
                              select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.ProductID = dto.ProductID;
            dt.ProductInfoID = dto.ProductInfoID;
            dt.QuantitySold = dto.QuantitySold;
            dt.SizeID = dto.SizeID;
            dt.UnitsInStock = dto.UnitsInStock;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoProducts dto, ref string err)
        {
            bool success;
            Product dt = (from n in dbs.Products
                              where n.ProductID == dto.ProductID
                              select n).Single();
            try
            {
                dbs.Products.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
