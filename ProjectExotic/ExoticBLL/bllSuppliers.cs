﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;


namespace ExoticBLL
{
    public class bllSuppliers
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get Supplier
        public IQueryable getSupplier()
        {
            var data = (from n in dbs.Suppliers
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoSuppliers dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            Supplier dt = new Supplier();
            //Nạp dữ liệu cho đối tượng
            dt.SupplierID = dto.SupplierID;
            dt.SupplierName = dto.SupplierName;
            dt.Address = dto.Address;
            dt.Country = dto.Country;
            dt.Phone = dto.Phone;
            dt.Fax = dto.Fax;
            dt.Email = dto.Email;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.Suppliers.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //UpdateData
        public bool updateData(dtoSuppliers dto, ref string err)
        {
            bool success;
            Supplier dt = (from n in dbs.Suppliers
                       where n.SupplierID == dto.SupplierID
                       select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.SupplierID = dto.SupplierID;
            dt.SupplierName = dto.SupplierName;
            dt.Address = dto.Address;
            dt.Country = dto.Country;
            dt.Phone = dto.Phone;
            dt.Fax = dto.Fax;
            dt.Email = dto.Email;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoSuppliers dto, ref string err)
        {
            bool success;
            Supplier dt = (from n in dbs.Suppliers
                       where n.SupplierID == dto.SupplierID
                       select n).Single();
            try
            {
                dbs.Suppliers.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
