﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDAL;
using ExoticDTO;

namespace ExoticBLL
{
   public class bllCustomerTypes
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get Table
            public IQueryable getTable()
            {
                var data = (from n in dbs.CustomerTypes
                            select n);
                return data;
            }
            //insert data
            public bool insertData(dtoCustomerTypes dto, ref string err)
            {
                bool sucess;
                // tao doi tuong rong
                CustomerType dt = new CustomerType();
                //nap du lieu cho doi tuong
                dt.CustomerTypeID=dto.CustomerTypeID;
                dt.CustomerTypeName=dto.CustomerTypeName;
                dt.DiscountRatio=dto.DiscountRatio;


            // nap doi tuong vao co so du lieu
            try
            {
                dbs.CustomerTypes.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                sucess = true;
            }
            //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
            catch(Exception error)
            {
                err = error.Message;
                sucess = false;
            }
            return sucess;
        }
            //update data
            public bool updateData(dtoCustomerTypes dto, ref string err)
            {
                bool sucess;
                CustomerType dt = (from n in dbs.CustomerTypes
                               where n.CustomerTypeID == dto.CustomerTypeID
                               select n).Single();
    
                //nap du lieu cho doi tuong
                dt.CustomerTypeID=dto.CustomerTypeID;
                dt.CustomerTypeName=dto.CustomerTypeName;
                dt.DiscountRatio=dto.DiscountRatio;

                // nap doi tuong vao co so du lieu
                try
                {
                    dbs.SubmitChanges();
                    sucess = true;
                }
                //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
                catch (Exception error)
                {
                    err = error.Message;
                    sucess = false;
                }
                return sucess;
            }
            // delete data
            public bool deleteData(dtoCustomerTypes dto, ref string err)
            {
                bool sucess;
                CustomerType dt = (from n in dbs.CustomerTypes
                               where n.CustomerTypeID == dto.CustomerTypeID
                               select n).Single();

                try
                {
                    dbs.CustomerTypes.DeleteOnSubmit(dt);
                    dbs.SubmitChanges();
                    sucess = true;
                }
                //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
                catch (Exception error)
                {
                    err = error.Message;
                    sucess = false;
                }
                return sucess;
            }
        }
    }
