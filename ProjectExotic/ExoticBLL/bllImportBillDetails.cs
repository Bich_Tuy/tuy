﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;


namespace ExoticBLL
{
    public class bllImportBillDetails
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get ImportBillDetail
        public IQueryable getImportBillDetail()
        {
            var data = (from n in dbs.ImportBillDetails
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoImportBillDetails dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            ImportBillDetail dt = new ImportBillDetail();
            //Nạp dữ liệu cho đối tượng
            dt.ImportBillID = dto.ImportBillID;
            dt.ProductID = dto.ProductID;
            dt.Quantity = dto.Quantity;
            dt.UnitPrice = dto.UnitPrice;
            dt.Totality = dto.Totality;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.ImportBillDetails.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //UpdateData
        public bool updateData(dtoImportBillDetails dto, ref string err)
        {
            bool success;
            ImportBillDetail dt = (from n in dbs.ImportBillDetails
                             where n.ImportBillID == dto.ImportBillID
                             select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.ImportBillID = dto.ImportBillID;
            dt.ProductID = dto.ProductID;
            dt.Quantity = dto.Quantity;
            dt.UnitPrice = dto.UnitPrice;
            dt.Totality = dto.Totality;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoImportBillDetails dto, ref string err)
        {
            bool success;
            ImportBillDetail dt = (from n in dbs.ImportBillDetails
                             where n.ImportBillID == dto.ImportBillID
                             select n).Single();
            try
            {
                dbs.ImportBillDetails.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
