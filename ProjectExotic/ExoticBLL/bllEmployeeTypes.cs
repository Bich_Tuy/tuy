﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;

namespace ExoticBLL
{
   public class bllEmployeeTypes
    {
       ShopCommerceDataContext dbs = new ShopCommerceDataContext();
            //Get Table
            public IQueryable getTable()
            {
                var data = (from n in dbs.EmployeeTypes
                            select n);
                return data;
            }
            //insert data
            public bool insertData(dtoEmployeeTypes dto, ref string err)
            {
                bool sucess;
                // tao doi tuong rong
                EmployeeType dt = new EmployeeType();
                //nap du lieu cho doi tuong
                dt.EmployeeTypeID=dto.EmployeeTypeID;
                dt.EmployeeTypeName=dto.EmployeeTypeName;
                dt.SalaryCoefficient=dto.SalaryCoefficient;
                

            // nap doi tuong vao co so du lieu
            try
            {
                dbs.EmployeeTypes.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                sucess = true;
            }
            //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
            catch(Exception error)
            {
                err = error.Message;
                sucess = false;
            }
            return sucess;
        }
            //update data
            public bool updateData(dtoEmployeeTypes dto, ref string err)
            {
                bool sucess;
                EmployeeType dt = (from n in dbs.EmployeeTypes
                               where n.EmployeeTypeID == dto.EmployeeTypeID
                               select n).Single();
    
                //nap du lieu cho doi tuong
                dt.EmployeeTypeID=dto.EmployeeTypeID;
                dt.EmployeeTypeName=dto.EmployeeTypeName;
                dt.SalaryCoefficient=dto.SalaryCoefficient;



                // nap doi tuong vao co so du lieu
                try
                {
                    dbs.SubmitChanges();
                    sucess = true;
                }
                //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
                catch (Exception error)
                {
                    err = error.Message;
                    sucess = false;
                }
                return sucess;
            }
            // delete data
            public bool deleteData(dtoEmployeeTypes dto, ref string err)
            {
                bool sucess;
                EmployeeType dt = (from n in dbs.EmployeeTypes
                               where n.EmployeeTypeID == dto.EmployeeTypeID
                               select n).Single();

                try
                {
                    dbs.EmployeeTypes.DeleteOnSubmit(dt);
                    dbs.SubmitChanges();
                    sucess = true;
                }
                //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
                catch (Exception error)
                {
                    err = error.Message;
                    sucess = false;
                }
                return sucess;
            }
        }
    }
