﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;

namespace ExoticBLL
{
    public class bllCustomers
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        public IQueryable getTable()
        {
            var data = (from n in dbs.Customers
                        select new
                        {
                            CustomerID=n.CustomerID,
                            CustomerTypeName=n.CustomerType.CustomerTypeName,
                            FirstName = n.FirstName,
                            LastName = n.LastName,
                            Gender=n.Gender,
                            IDNumber = n.IDNumber,
                            Address = n.Address,
                            Phone = n.Phone,
                            Phone1 = n.Phone1,
                            RegisterDate = n.RegisterDate,
                            Email = n.Email

                        });
            return data;
        }

        
        
        //insert data
            public bool insertData(dtoCustomers dto, ref string err)
            {
                bool sucess;
                // tao doi tuong rong
                Customer dt = new Customer();
                //nap du lieu cho doi tuong
                dt.Address=dto.Address;
                dt.CustomerID=dto.CustomerID;
                dt.CustomerTypeID=dto.CustomerTypeID;
                dt.Email=dto.Email;
                dt.FirstName=dto.FirstName;
                dt.IDNumber=dto.IDNumber;
                dt.LastName=dto.LastName;
                dt.Phone=dto.Phone;
                dt.Phone1=dto.Phone1;
                dt.RegisterDate=dto.RegisterDate;
                dt.Gender = dto.Gender;
               

             //nap doi tuong vao co so du lieu
            try
            {
                dbs.Customers.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                sucess = true;
            }
            //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
            catch (Exception error)
            {
                err = error.Message;
                sucess = false;
            }
                return sucess;
        }
            //update data
            public bool updateData(dtoCustomers dto, ref string err)
            {
                bool sucess;
                Customer dt = (from n in dbs.Customers
                               where n.CustomerID == dto.CustomerID
                               select n).Single();
    
                //nap du lieu cho doi tuong
                dt.CustomerID=dto.CustomerID;
                dt.CustomerTypeID=dto.CustomerTypeID;
                dt.Email=dto.Email;
                dt.FirstName=dto.FirstName;
                dt.IDNumber=dto.IDNumber;
                dt.LastName=dto.LastName;
                dt.Phone=dto.Phone;
                dt.Phone1=dto.Phone1;
                dt.RegisterDate=dto.RegisterDate;
                dt.Address = dto.Address;
                dt.Gender = dto.Gender;

                // nap doi tuong vao co so du lieu
                try
                {
                    dbs.SubmitChanges();
                    sucess = true;
                }
                //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
                catch (Exception error)
                {
                    err = error.Message;
                    sucess = false;
                }
                return sucess;
            }
            // delete data
            public bool deleteData(dtoCustomers dto, ref string err)
            {
                bool sucess;
                Customer dt = (from n in dbs.Customers
                               where n.CustomerID == dto.CustomerID
                               select n).Single();

                try
                {
                    dbs.Customers.DeleteOnSubmit(dt);
                    dbs.SubmitChanges();
                    sucess = true;
                }
                //tra ve cau bao loi neu khong the insert doi tuong vao du lieu
                catch (Exception error)
                {
                    err = error.Message;
                    sucess = false;
                }
                return sucess;
            }
        }
    }
