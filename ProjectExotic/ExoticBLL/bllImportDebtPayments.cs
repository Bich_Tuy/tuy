﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;


namespace ExoticBLL
{
    public class bllImportDebtPayments
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get ImportDebtPayment
        public IQueryable getImportDebtPayment()
        {
            var data = (from n in dbs.ImportDebtPayments
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoImportDebtPayments dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            ImportDebtPayment dt = new ImportDebtPayment();
            //Nạp dữ liệu cho đối tượng
            dt.ImportBillID = dto.ImportBillID;
            dt.Money = dto.Money;
            dt.Time = dto.Time;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.ImportDebtPayments.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //UpdateData
        public bool updateData(dtoImportDebtPayments dto, ref string err)
        {
            bool success;
            ImportDebtPayment dt = (from n in dbs.ImportDebtPayments
                           where n.ID == dto.ID
                           select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.ImportBillID = dto.ImportBillID;
            dt.Money = dto.Money;
            dt.Time = dto.Time;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoImportDebtPayments dto, ref string err)
        {
            bool success;
            ImportDebtPayment dt = (from n in dbs.ImportDebtPayments
                                    where n.ID == dto.ID
                           select n).Single();
            try
            {
                dbs.ImportDebtPayments.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
