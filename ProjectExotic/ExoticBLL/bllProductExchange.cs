﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDAL;
using ExoticDTO;

namespace ExoticBLL
{
    public class bllProductExchange
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        public IQueryable getTable()
        {
            var data = (from n in dbs.ProductExchanges select n);
            return data;
        }
        //insert data

        public bool insertData(dtoProductExchange dto, ref string err)
        {
            bool success;
            //tao doi tuong rong
            ProductExchange dt = new ProductExchange();
            dt.ExportBillID = dto.ExportBillID;
            dt.OldProductID = dto.OldProductID;
            dt.NewProductID = dto.NewProductID;
            dt.Quantity = dto.Quantity;
            dt.Time = dto.Time;

            try
            {
                dbs.ProductExchanges.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }

        public bool updateData(dtoProductExchange dto, ref string err)
        {
            bool success;
            ProductExchange dt = (from n in dbs.ProductExchanges
                                   where n.ExportBillID == dto.ExportBillID
                                    && n.OldProductID == dto.OldProductID
                                    && n.NewProductID == dto.NewProductID
                                   select n).Single();
            //nap du lieu cho doi tuong
            dt.ExportBillID = dto.ExportBillID;
            dt.OldProductID = dto.OldProductID;
            dt.NewProductID = dto.NewProductID;
            dt.Quantity = dto.Quantity;
            dt.Time = dto.Time;
            try
            {
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }

        //dalete data
        public bool deleteData(dtoProductExchange dto, ref string err)
        {
            bool success;
            ProductExchange dt = (from n in dbs.ProductExchanges
                                  where n.ExportBillID == dto.ExportBillID
                                   && n.OldProductID == dto.OldProductID
                                   && n.NewProductID == dto.NewProductID
                                   select n).Single();
            //xoa du lieu

            try
            {
                dbs.ProductExchanges.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
