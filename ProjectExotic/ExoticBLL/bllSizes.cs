﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;


namespace ExoticBLL
{
    public class bllSizes
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get table
        public IQueryable getTable()
        {
            var data = (from n in dbs.Sizes
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoSizes dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            Size dt = new Size();
            //Nạp dữ liệu cho đối tượng
            dt.SizeID = dto.SizeID;
            dt.SizeName = dto.SizeName;
            dt.Categories = dto.Categories;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.Sizes.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //UpdateData
        public bool updateData(dtoSizes dto, ref string err)
        {
            bool success;
            Size dt = (from n in dbs.Sizes
                          where n.SizeID == dto.SizeID
                          select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.SizeID = dto.SizeID;
            dt.SizeName = dto.SizeName;
            dt.Categories = dto.Categories;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoSizes dto, ref string err)
        {
            bool success;
            Size dt = (from n in dbs.Sizes
                          where n.SizeID == dto.SizeID
                          select n).Single();
            try
            {
                dbs.Sizes.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
