﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;

namespace ExoticBLL
{
    public class bllProductInfos
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get table
        public IQueryable getTable()
        {
            var data = (from n in dbs.ProductInfos
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoProductInfos dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            ProductInfo dt = new ProductInfo();
            //Nạp dữ liệu cho đối tượng
            dt.BrandID = dto.BrandID;
            dt.CalculationUnit = dto.CalculationUnit;
            dt.CategoryID = dto.CategoryID;
            dt.Color = dto.Color;
            dt.Description = dto.Description;
            dt.Material = dto.Material;
            dt.ProductInfoID = dto.ProductInfoID;
            dt.RetailPrice = dto.RetailPrice;
            dt.WholesalePrice = dto.WholesalePrice;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.ProductInfos.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }
             
            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch(Exception error)
            {
                err = error.Message;
                success = false;
            }
             return success;
        }
        //UpdateData
        public bool updateData(dtoProductInfos dto, ref string err)
        {
            bool success;
            ProductInfo dt = (from n in dbs.ProductInfos
                              where n.ProductInfoID == dto.ProductInfoID
                              select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.BrandID = dto.BrandID;
            dt.CalculationUnit = dto.CalculationUnit;
            dt.CategoryID = dto.CategoryID;
            dt.Color = dto.Color;
            dt.Description = dto.Description;
            dt.Material = dto.Material;
            dt.ProductInfoID = dto.ProductInfoID;
            dt.RetailPrice = dto.RetailPrice;
            dt.WholesalePrice = dto.WholesalePrice;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoProductInfos dto, ref string err)
        {
            bool success;
            ProductInfo dt = (from n in dbs.ProductInfos
                              where n.ProductInfoID == dto.ProductInfoID
                              select n).Single();
            try
            {
                dbs.ProductInfos.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
