﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;

namespace ExoticBLL
{
    public class bllBrands
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get table
        public IQueryable getTable()
        {
            dbs = new ShopCommerceDataContext();
            var data = (from n in dbs.Brands
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoBrands dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            Brand dt = new Brand();
            //Nạp dữ liệu cho đối tượng
            dt.BrandID = dto.BrandID;
            dt.BrandName = dto.BrandName;
            dt.Country = dto.Country;
            dt.Description = dto.Description;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.Brands.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //UpdateData
        public bool updateData(dtoBrands dto, ref string err)
        {
            bool success;
            Brand dt = (from n in dbs.Brands
                           where n.BrandID == dto.BrandID
                           select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.BrandID = dto.BrandID;
            dt.BrandName = dto.BrandName;
            dt.Country = dto.Country;
            dt.Description = dto.Description;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoBrands dto, ref string err)
        {
            bool success;
            Brand dt = (from n in dbs.Brands
                           where n.BrandID == dto.BrandID
                           select n).Single();
            try
            {
                dbs.Brands.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
