﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDAL;
using ExoticDTO;

namespace ExoticBLL
{
    public class bllExportDebtPayments
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        public IQueryable getTable()
        {
            var data = (from n in dbs.ExportDebtPayments select n);
            return data;
        }
        //insert data

        public bool insertData(dtoExportDebtPayments dto, ref string err)
        {
            bool success;
            //tao doi tuong rong
            ExportDebtPayment dt = new ExportDebtPayment();
            dt.ExportBillID = dto.ExportBillID;
            dt.Money = dto.Money;
            dt.Time = dto.Time;

            try
            {
                dbs.ExportDebtPayments.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }

        public bool updateData(dtoExportDebtPayments dto, ref string err)
        {
            bool success;
            ExportDebtPayment dt = (from n in dbs.ExportDebtPayments
                                   where n.ID == dto.ID
                                   select n).Single();
            //nap du lieu cho doi tuong
            dt.ExportBillID = dto.ExportBillID;
            dt.Money = dto.Money;
            dt.Time = dto.Time;
            try
            {
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }

        //dalete data
        public bool deleteData(dtoExportDebtPayments dto, ref string err)
        {
            bool success;
            ExportDebtPayment dt = (from n in dbs.ExportDebtPayments
                                    where n.ID == dto.ID
                                   select n).Single();
            //xoa du lieu

            try
            {
                dbs.ExportDebtPayments.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
