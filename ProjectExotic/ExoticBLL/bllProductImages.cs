﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;

namespace ExoticBLL
{
    public class bllProductImages
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get table
        public IQueryable getTable()
        {
            var data = (from n in dbs.ProductImages
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoProductImages dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            ProductImage dt = new ProductImage();
            //Nạp dữ liệu cho đối tượng
            dt.ProductInfoID = dto.ProductInfoID;
            dt.Image = dto.Image;
            dt.Description = dto.Description;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.ProductImages.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //UpdateData
        public bool updateData(dtoProductImages dto, ref string err)
        {
            bool success;
            ProductImage dt = (from n in dbs.ProductImages
                        where n.ProductImageID == dto.ProductImageID
                        select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.ProductInfoID = dto.ProductInfoID;
            dt.Image = dto.Image;
            dt.Description = dto.Description;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoProductImages dto, ref string err)
        {
            bool success;
            ProductImage dt = (from n in dbs.ProductImages
                               where n.ProductImageID == dto.ProductImageID
                        select n).Single();
            try
            {
                dbs.ProductImages.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
