﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDAL;
using ExoticDTO;

namespace ExoticBLL
{
    public class bllExportBills
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        public IQueryable getTable()
        {
            var data = (from n in dbs.ExportBills select n);
            return data;
        }
        //insert data

        public bool insertData(dtoExportBills dto, ref string err)
        {
            bool success;
            //tao doi tuong rong
            ExportBill dt = new ExportBill();
            dt.ExportBillID = dto.ExportBillID;
            dt.CustomerID = dto.CustomerID;
            dt.EmployeeID = dto.EmployeeID;
            dt.Time = dto.Time;
            dt.IsInDebt = dto.IsInDebt;
            dt.DebtDeadline = dto.DebtDeadline;

            try
            {
                dbs.ExportBills.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }
            catch(Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }

        public bool updateData(dtoExportBills dto, ref string err)
        {
            bool success;
            ExportBill dt = (from n in dbs.ExportBills
                             where n.ExportBillID == dto.ExportBillID
                             select n).Single();
            //nap du lieu cho doi tuong
            dt.ExportBillID = dto.ExportBillID;
            dt.CustomerID = dto.CustomerID;
            dt.EmployeeID = dto.EmployeeID;
            dt.Time = dto.Time;
            dt.IsInDebt = dto.IsInDebt;
            dt.DebtDeadline = dto.DebtDeadline;
            try
            {
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }

        //dalete data
        public bool deleteData(dtoExportBills dto, ref string err)
        {
            bool success;
            ExportBill dt = (from n in dbs.ExportBills
                             where n.ExportBillID == dto.ExportBillID
                             select n).Single();
            //xoa du lieu

            try
            {
                dbs.ExportBills.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
    

}
