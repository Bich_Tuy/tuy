﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;

namespace ExoticBLL
{
    public class bllCategories
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get table
        public IQueryable getTable()
        {
            var data = (from n in dbs.Categories
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoCategories dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            Category dt = new Category();
            //Nạp dữ liệu cho đối tượng
            dt.CategoryID = dto.CategoryID;
            dt.CategoryName = dto.CategoryName;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.Categories.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //UpdateData
        public bool updateData(dtoCategories dto, ref string err)
        {
            bool success;
            Category dt = (from n in dbs.Categories
                       where n.CategoryID == dto.CategoryID
                       select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.CategoryID = dto.CategoryID;
            dt.CategoryName = dto.CategoryName;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoCategories dto, ref string err)
        {
            bool success;
            Category dt = (from n in dbs.Categories
                       where n.CategoryID == dto.CategoryID
                       select n).Single();
            try
            {
                dbs.Categories.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
