﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExoticDTO;
using ExoticDAL;


namespace ExoticBLL
{
    public class bllImportBills
    {
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();
        //Get ImportBill
        public IQueryable getImportBill()
        {
            var data = (from n in dbs.ImportBills
                        select n);
            return data;
        }
        //InsertData
        public bool insertData(dtoImportBills dto, ref string err)
        {
            bool success;
            //Tạo đối tượng rỗng
            ImportBill dt = new ImportBill();
            //Nạp dữ liệu cho đối tượng
            dt.ImportBillID = dto.ImportBillID;
            dt.SupplierID = dto.SupplierID;
            dt.EmployeeID = dto.EmployeeID;
            dt.Time = dto.Time;
            dt.IsInDebt = dto.IsInDebt;
            dt.DebtDeadline = dto.DebtDeadline;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.ImportBills.InsertOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //UpdateData
        public bool updateData(dtoImportBills dto, ref string err)
        {
            bool success;
            ImportBill dt = (from n in dbs.ImportBills
                                    where n.ImportBillID == dto.ImportBillID
                                    select n).Single();
            //Nạp dữ liệu cho đối tượng
            dt.ImportBillID = dto.ImportBillID;
            dt.SupplierID = dto.SupplierID;
            dt.EmployeeID = dto.EmployeeID;
            dt.Time = dto.Time;
            dt.IsInDebt = dto.IsInDebt;
            dt.DebtDeadline = dto.DebtDeadline;
            //Nạp đối tượng vào cơ sở dữ liệu
            try
            {
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
        //Delete Data
        public bool deleteData(dtoImportBills dto, ref string err)
        {
            bool success;
            ImportBill dt = (from n in dbs.ImportBills
                                    where n.ImportBillID == dto.ImportBillID
                                    select n).Single();
            try
            {
                dbs.ImportBills.DeleteOnSubmit(dt);
                dbs.SubmitChanges();
                success = true;
            }

            //Trả về câu báo lỗi nếu không thể insert đối tượng vào dữ liệu
            catch (Exception error)
            {
                err = error.Message;
                success = false;
            }
            return success;
        }
    }
}
