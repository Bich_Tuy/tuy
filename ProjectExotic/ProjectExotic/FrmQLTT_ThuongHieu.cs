﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticBLL;
using ExoticDTO;

namespace ProjectExotic
{
    public partial class FrmQLTT_ThuongHieu : DevExpress.XtraEditors.XtraForm
    {

        #region Các biến - Hàm
        bllBrands bll = null;//Gọi lớp BLL
        dtoBrands dto = null;
        public static string brandID; //Biến dùng khi Cập Nhật Thương Hiệu
        public static bool them; //Biến nhận biết là đang thêm hay cập nhật dữ liệu. 
                    // True là click vào button thêm
                    //False là click vào button update
        
        //Load dữ liệu
        void loadData()
        {
            bll = new bllBrands();
            gridControl1.DataSource = bll.getTable();
        }
        //Binding dữ liệu lên gridview
        void bindingData()
        {
            try
            {
                lblBrandID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["BrandID"]).ToString();
                lblBrandName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["BrandName"]).ToString();
                lblCountry.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Country"]).ToString();
                lblDescription.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Description"]).ToString();
            }
            //Sự kiện exception này để tránh tình trạng xảy ra lỗi khi một ô dữ liệu = null
            catch (NullReferenceException)
            {

            }
            catch (InvalidCastException)
            { }
        }
        //Thêm dữ liệu
        void addData()
        {
            them = true;
            FrmThemCapNhat_ThuongHieu frm = new FrmThemCapNhat_ThuongHieu();
            frm.Text = "Thêm Thương Hiệu";
            frm.ShowDialog();
            FrmQLTT_ThuongHieu_Load(null, null);
            focusInARow(frm.brandID);
        }
        //Cập nhật dữ liệu
        void updateData()
        {
            them = false;
            if (gridView1.SelectedRowsCount == 0)
            {
                MessageBox.Show("Bạn phải chọn một thương hiệu để cập nhật", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                FrmThemCapNhat_ThuongHieu frm = new FrmThemCapNhat_ThuongHieu();
                brandID = lblBrandID.Text;
                frm.Text = "Cập Nhật Thương Hiệu";
                frm.ShowDialog();
                FrmQLTT_ThuongHieu_Load(null, null);
                focusInARow(frm.brandID);
            }
            
        }
        //Xóa dữ liệu
        void deleteData()
        {
            if (gridView1.SelectedRowsCount == 0)
            {

            }
            else
            {
                dto = new dtoBrands();
                bll = new bllBrands();
                int r = gridView1.FocusedRowHandle;
                dto.BrandID = gridView1.GetRowCellValue(r, gridView1.Columns["BrandID"]).ToString();
                DialogResult dialog = MessageBox.Show("Bạn thật sự muốn xóa thương hiệu này không?", "Cảnh báo",
                   MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (dialog == DialogResult.OK)
                {
                    string err = "";
                    bool f = bll.deleteData(dto, ref err);
                    if(!f)
                    {
                        MessageBox.Show("Xóa không thành công. Lỗi : "+err, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    FrmQLTT_ThuongHieu_Load(null, null);
                }
            }
        }
        ////Hàm này dùng để focus vào dòng với được thêm vào
        void focusInARow(string variable)
        {
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (variable == gridView1.GetRowCellValue(i, gridView1.Columns["BrandID"]).ToString())
                {
                    gridView1.FocusedRowHandle = i;
                    break;
                }
            }
        }
        #endregion
        #region Các sự kiện
        public FrmQLTT_ThuongHieu()
        {
            InitializeComponent();
        }

        private void FrmQLTT_ThuongHieu_Load(object sender, EventArgs e)
        {

            loadData();
            gridView1_Click(null, null);
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            bindingData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            addData();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            updateData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteData();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            FrmQLTT_ThuongHieu_Load(null, null);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            updateData();
        }
            #endregion




    }
}