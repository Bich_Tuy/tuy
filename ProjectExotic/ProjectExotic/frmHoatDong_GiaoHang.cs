﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticBLL;
using ExoticDAL;
using ExoticDTO;

namespace ProjectExotic
{
    public partial class frmHoatDong_GiaoHang : DevExpress.XtraEditors.XtraForm
    {
        public static int shipmentID;
        ShopCommerceDataContext dbs = null;
        bllShipments bll = null;
        dtoShipments dto = null;
        
        void loadData()
        {
            bll = new bllShipments();
            gridControl1.DataSource = bll.getTable();
        }

        void bindingData()
        {
            try
            {
                lblShipmentID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["ShipmentID"]).ToString();
                lblExportBillID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["ExportBillID"]).ToString();
                lblCustomerName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["CustomerName"]).ToString();
                lblEmployeeName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["EmployeeName"]).ToString();
                lblAddress.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Address"]).ToString();
                lblPhone.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone"]).ToString();
                lblPhone1.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone1"]).ToString();
                lblShippingPrice.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["ShippingPrices"]).ToString();
                lblTime.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Time"]).ToString();
            }
            catch(NullReferenceException)
            {

            }
            catch(InvalidCastException)
            { }
        }

        void addData()
        {
            frmThemCapNhat_PGH frm = new frmThemCapNhat_PGH();
            frm.Text = "Thêm Phiếu Giao Hàng";
            frm.ShowDialog();
            frmHoatDong_GiaoHang_Load(null, null);
            focusInARow(frm.shipmentID);
        }
        void focusInARow(int var)
        {
            for(int i=0;i<gridView1.RowCount;i++)
            {
                if (var ==Convert.ToInt32( gridView1.GetRowCellValue(i, gridView1.Columns["ShipmentID"]).ToString()))
                {
                    gridView1.FocusedRowHandle = i;
                    break;
                }
                  
            }
        }
        public frmHoatDong_GiaoHang()
        {
            InitializeComponent();
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            bindingData();
        }

        private void frmHoatDong_GiaoHang_Load(object sender, EventArgs e)
        {
            loadData();
            gridView1_Click(null, null);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            addData();
        }

    }
}