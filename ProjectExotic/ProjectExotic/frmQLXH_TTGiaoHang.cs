﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticBLL;
using ExoticDAL;
using ExoticDTO;

namespace ProjectExotic
{
    public partial class frmQLXH_TTGiaoHang : DevExpress.XtraEditors.XtraForm
    {
        ShopCommerceDataContext dbs = null;
        bllShipments bll = null;
        dtoShipments dto = null;
        public static int shipmentID;// biến dùng cập nhật phiếu giao hàng
        public static bool them;
        // True là click vào button thêm
        //False là click vào button update

        void loadData()
        {
            bll = new bllShipments();
            gridControl1.DataSource = bll.getTable();
        }

        void bindingData()
        {
            try
            {
                lblShipmentID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["ShipmentID"]).ToString();
                lblExportBillID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["ExportBillID"]).ToString();
                lblCustomerName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["CustomerName"]).ToString();
                lblEmployeeName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["EmployeeName"]).ToString();
                lblAddress.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Address"]).ToString();
                lblPhone.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone"]).ToString();
                lblPhone1.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone1"]).ToString();
                lblShippingPrice.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["ShippingPrices"]).ToString();
                lblTime.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Time"]).ToString();
            }
            catch(NullReferenceException)
            { }
            catch(InvalidCastException)
            { }
        }

        void addData()
        {
            them = true;
            frmThemCapNhat_PGH frm = new frmThemCapNhat_PGH();
            frm.Text = "Thêm Phiếu Giao Hàng";
            frm.ShowDialog();
            frmQLXH_TTGiaoHang_Load(null, null);
        }
        void updateData()
        {
            them = false;
            if(gridView2.SelectedRowsCount==0)
            {
                MessageBox.Show("Bạn phải chọn một thương hiệu để cập nhật", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                frmThemCapNhat_PGH frm = new frmThemCapNhat_PGH();
                shipmentID =Convert.ToInt32( lblShipmentID.Text);
                frm.Text = "Cập Nhật Phiếu Giao Hàng";
                frm.ShowDialog();
                frmQLXH_TTGiaoHang_Load(null, null);
                
            }
        }
        void deleteData()
        {
            if(gridView2.SelectedRowsCount==0)
            {
                MessageBox.Show("Bạn phải chọn một thương hiệu để xóa", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dto = new dtoShipments();
                bll = new bllShipments();
                int r = gridView2.FocusedRowHandle;
                dto.ShipmentID =Convert.ToInt32( gridView2.GetRowCellValue(r, gridView2.Columns["ShipmentID"]).ToString());
                DialogResult dialog = MessageBox.Show("Bạn thật sự muốn xóa Phiếu Giao Hàng này không?", "Cảnh báo",
                   MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if(dialog==DialogResult.OK)
                {
                    string err = "";
                    bool f = bll.deleteData(dto, ref err);
                    if(!f)
                    {
                        MessageBox.Show("Xóa không thành công. Lỗi : " + err, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    frmQLXH_TTGiaoHang_Load(null, null);
                }
            }
        }
        void focusInARow(string var)
        {
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (var == gridView1.GetRowCellValue(i, gridView1.Columns["ShipmentID"]).ToString())
                {
                    gridView2.FocusedRowHandle = i;
                    break;
                }

            }
        }
        public frmQLXH_TTGiaoHang()
        {
            InitializeComponent();
        }

        private void frmQLXH_TTGiaoHang_Load(object sender, EventArgs e)
        {
            loadData();
            gridView2_Click(null, null);
        }

        private void gridView2_Click(object sender, EventArgs e)
        {
            bindingData();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            addData();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            updateData();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            deleteData();
        }

        private void btnTaiLai_Click(object sender, EventArgs e)
        {
            frmQLXH_TTGiaoHang_Load(null, null);
        }
    }
}