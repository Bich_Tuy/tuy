﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticDTO;
using ExoticBLL;
using ExoticDAL;

namespace ProjectExotic
{
    public partial class FrmThemCapNhat_ThuongHieu : DevExpress.XtraEditors.XtraForm
    {
        #region Các thuộc tính và hàm
        public string brandID;//Thuộc tính để truyền vào form chính
        ShopCommerceDataContext dbs = null;
        dtoBrands dto = null;
        bllBrands bll = null;
        bool them = FrmQLTT_ThuongHieu.them;


        //Đặt trạng thái Form khi thêm dữ liệu / cập nhật dữ liệu
        void setPanel()
        {
            if (them == true)
            {
                txtBrandID.Enabled = true;
            }
            else
            {
                txtBrandID.Enabled = false;
                dbs = new ShopCommerceDataContext();
                //Truyền dữ liệu
                brandID = FrmQLTT_ThuongHieu.brandID;
                //Tạo đối tượng
                Brand data = (from n in dbs.Brands
                              where n.BrandID == FrmQLTT_ThuongHieu.brandID
                              select n).First();
                //Binding dữ liệu
                txtBrandID.Text = data.BrandID;
                txtBrandName.Text = data.BrandName;
                txtCountry.Text = data.Country;
                txtDescription.Text = data.Description;
            }
        }


        //Kiểm tra dữ liệu
        void checkData()
        {
            bool right = true;//Biến dùng để kiểm tra dữ liệu
            if (txtBrandID.Text.Length > 3)
            {
                right = false;
                MessageBox.Show("Độ dài của BrandID không được quá 3 kí tự. \nChúng tôi khuyên bạn điền BrandID gồm 3 chữ cái in hoa ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (txtBrandName.Text.Length > 50)
            {
                right = false;
                MessageBox.Show("Tên thương hiệu không được quá 50 kí tự", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (txtCountry.Text.Length > 20)
            {
                right = false;
                MessageBox.Show("Tên lãnh thổ không được quá 20 kí tự", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (txtDescription.Text.Length > 100)
            {
                right = false;
                MessageBox.Show("Thông Tin Thêm không được quá 100 kí tự", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (right)
            {
                if (them)
                {
                    addData();
                }
                else
                {
                    updateData();
                }
            }

        }
        //Thêm dữ liệu
        void addData()
        {
            dto = new dtoBrands();
            bll = new bllBrands();
            string err = "";
            //Nạp dữ liệu
            dto.BrandID = txtBrandID.Text.Trim();
            dto.BrandName = txtBrandName.Text.Trim();
            dto.Country = txtCountry.Text.Trim();
            dto.Description = txtDescription.Text.Trim();
            bool f = bll.insertData(dto, ref err);
            if (f)
            {
                MessageBox.Show("Thêm dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                brandID = dto.BrandID;
                this.Close();
            }
            else
            {
                MessageBox.Show("Thêm dữ liệu thất bại", "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtBrandName.Focus();
            }
        }
        //Cập nhật dữ liệu
        void updateData()
        {
            dto = new dtoBrands();
            bll = new bllBrands();
            string err = "";
            //Nạp dữ liệu
            dto.BrandID = txtBrandID.Text.Trim();
            dto.BrandName = txtBrandName.Text.Trim();
            dto.Country = txtCountry.Text.Trim();
            dto.Description = txtDescription.Text.Trim();
            bool f = bll.updateData(dto, ref err);
            if (f)
            {
                MessageBox.Show("Cập nhật dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Cập nhật dữ liệu thất bại", "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtBrandName.Focus();
            }
        }

        #endregion
        #region Các phương thức
        public FrmThemCapNhat_ThuongHieu()
        {
            InitializeComponent();
        }

        private void txtBrandID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtBrandName.Focus();
        }

        private void txtBrandName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCountry.Focus();
        }

        private void txtCountry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDescription.Focus();
        }

        private void FrmThemCapNhat_ThuongHieu_Load(object sender, EventArgs e)
        {
            setPanel();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            checkData();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Có thể bạn chưa lưu dữ liệu.\nBạn có thật sự muốn thoát hay không?", "Thoát",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialog == DialogResult.OK)
            {
                this.Dispose();
                this.Close();
            }
            else
            {
                txtBrandID.Focus();
            }

        }
        #endregion 
    }
}