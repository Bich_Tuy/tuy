﻿using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.Skins.Info;
using DevExpress.XtraBars.Docking2010.Customization;
using System;
using System.Linq;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Internal;
using System.Reflection;
using System.IO;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;

namespace ProjectExotic
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            //((DevExpress.LookAndFeel.Design.UserLookAndFeelDefault)DevExpress.LookAndFeel.Design.UserLookAndFeelDefault.Default).LoadSettings(() =>
            //{
            //    var skinCreator = new SkinBlobXmlCreator("HybridApp",
            //        "DevExpress.HybridApp.Win.SkinData.", typeof(Program).Assembly, null);
            //    SkinManager.Default.RegisterSkin(skinCreator);
            //});
            //AsyncAdornerBootStrapper.RegisterLookAndFeel(
            //    "MetroBlack", "DevExpress.RealtorWorld.Win.SkinData.", typeof(Program).Assembly);
            UserLookAndFeel.Default.SetSkinStyle("Office 2013");
            SkinManager.EnableFormSkins();
            BonusSkins.Register();
            Application.Run(new MainForm());
          
        }
    }
}

