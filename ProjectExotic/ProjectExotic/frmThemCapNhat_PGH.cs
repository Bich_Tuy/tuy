﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticDTO;
using ExoticDAL;
using ExoticBLL;

namespace ProjectExotic
{
    public partial class frmThemCapNhat_PGH : DevExpress.XtraEditors.XtraForm
    {
        public int shipmentID;// thuộc tính dùng để truyền vào form chính
        ShopCommerceDataContext dbs = null;
        dtoShipments dto = null;
        bllShipments bll = null;
        bool them = frmQLXH_TTGiaoHang.them;

        // đặt trạng thái form
        void setPanel()
        {
            if(them==true)
            {
                txtShipmentID.Enabled = true;
            }
            else
            {
                txtShipmentID.Enabled = false;
                dbs = new ShopCommerceDataContext();
                shipmentID = frmQLXH_TTGiaoHang.shipmentID;
                // tạo đối tượng
                Shipment data = (from n in dbs.Shipments
                                 where n.ShipmentID == frmQLXH_TTGiaoHang.shipmentID
                                 select n).First();
                txtShipmentID.Text =Convert.ToString(data.ShipmentID);
                txtExportBillID.Text = data.ExportBillID;
                txtAddress.Text = data.Address;
                txtPhone.Text = data.Phone;
                txtPhone1.Text = data.Phone1;
                txtShippingPrices.Text =Convert.ToString(data.ShippingPrices);
            }

        }
        void checkData()
        {
            bool right = true;//Biến dùng để kiểm tra dữ liệu
            if (txtExportBillID.Text.Length>20)
            {
                right = false;
                MessageBox.Show("Độ dài của mã hợp đồng xuất hàng không được quá 20 kí tự.  ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (right)
            {
                if (them)
                {
                    addData();
                }
                else
                {
                    updateData();
                }
            }
        }

        void addData()
        {
            dto = new dtoShipments();
            bll = new bllShipments();
            string err = "";
            dto.ShipmentID =Convert.ToInt32(txtShipmentID.Text.Trim());
            dto.ExportBillID = txtExportBillID.Text.Trim();
            dto.CustomerID = cbbCustomerName.ValueMember;
            dto.EmployeeID = cbbEmployeeName.ValueMember;
            dto.Adress = txtAddress.Text.Trim();
            dto.Phone = txtPhone.Text.Trim();
            dto.Phone1 = txtPhone1.Text.Trim();
            dto.Time = Convert.ToDateTime(daTime.Value);
            dto.ShippingPrices = Convert.ToDecimal(txtShippingPrices.Text);
            bool f = bll.insertData(dto, ref err);
            if (f == true)
            {
                MessageBox.Show("Thêm dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                shipmentID = dto.ShipmentID;
                this.Close();
            }
            else
            {
                MessageBox.Show("Thêm dữ liệu thất bại" + err, "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void updateData()
        {

            dto = new dtoShipments();
            bll = new bllShipments();
            string err = "";
            // nạp dữ liệu
            dto.ShipmentID = Convert.ToInt32(txtShipmentID.Text.Trim());
            dto.ExportBillID = txtExportBillID.Text.Trim();
            dto.CustomerID = cbbCustomerName.ValueMember;
            dto.EmployeeID = cbbEmployeeName.ValueMember;
            dto.Adress = txtAddress.Text.Trim();
            dto.Phone = txtPhone.Text.Trim();
            dto.Phone1 = txtPhone1.Text.Trim();
            dto.Time = Convert.ToDateTime(daTime.Value);
            dto.ShippingPrices = Convert.ToDecimal(txtShippingPrices.Text);
            bool f = bll.updateData(dto, ref err);
            if (f == true)
            {
                MessageBox.Show("cập nhật dữ liệu thành công" + err, "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                shipmentID =dto.ShipmentID;
                this.Close();
            }
            else
            {
                MessageBox.Show("cập nhật dữ liệu thất bại", "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
       
        public frmThemCapNhat_PGH()
        {
            InitializeComponent();
        }

        private void frmThemCapNhat_PGH_Load(object sender, EventArgs e)
        {
            dbs = new ShopCommerceDataContext();
            // đổ dữ liệu vào combobox
            var em = from n in dbs.Employees
                     select new
                     {
                         EmployeeID = n.EmployeeID,
                         EmployeeName = (n.FirstName + " " + n.LastName).Trim()
                     };
            cbbEmployeeName.DataSource = em;
            cbbEmployeeName.DisplayMember = "EmployeeName";
            cbbEmployeeName.ValueMember = "EmployeeID";
            var data1 = from n in dbs.Customers
                        select new { CustomerID=n.CustomerID,CustomerName=(n.FirstName+" "+n.LastName).Trim()
                        };
            cbbCustomerName.DataSource = data1;
            cbbCustomerName.DisplayMember = "CustomerName";
            cbbCustomerName.ValueMember = "CustomerID";
            if (this.Text == "Thêm Phiếu Giao Hàng")
            {
                them = true;
            }
            else
            {
                them = false;
                setPanel();
            }
        }
        #region thừa
        private void cbbCustomerID_Click(object sender, EventArgs e)
        {
        }

        private void cbbEmployeeID_Click(object sender, EventArgs e)
        {
        }
        private void cbbCustomerID_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void cbbEmployeeID_SelectedValueChanged(object sender, EventArgs e)
        {
            //Employee employee = (from n in dbs.Employees
            //                     where n.EmployeeID==Convert.ToString(cbbEmployeeID.SelectedValue)
            //                     select n).First();
            //txtEmployeeName.Text = employee.LastName;
        }
        #endregion

        private void btnHuy_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Có thể bạn chưa lưu dữ liệu.\nBạn có thật sự muốn thoát hay không?", "Thoát",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialog == DialogResult.OK)
            {
                this.Dispose();
                this.Close();
            }
            else
            {
                txtShipmentID.Focus();
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            checkData();
        }

    }
}