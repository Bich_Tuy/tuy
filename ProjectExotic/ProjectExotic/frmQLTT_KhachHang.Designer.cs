﻿namespace ProjectExotic
{
    partial class frmQLTT_KhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQLTT_KhachHang));
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gridControlQLTT_KhachHang = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.CustomerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CustomerTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IDNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Gender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Address = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Phone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Phone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Email = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RegisterDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.xtraUserControl1 = new DevExpress.XtraEditors.XtraUserControl();
            this.lblCustomerType = new System.Windows.Forms.Label();
            this.lblCustomerID = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblGender = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblRegisterDate = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblIDNumber = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPhone1 = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.CustomerTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CusTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DiscountRatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnReload = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnPrintCus = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpdateCus = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddCus = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnDelCus = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnUpdateCusType = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelCusType = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddCusType = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCusID = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlQLTT_KhachHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(944, 485);
            this.panel3.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(236, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(472, 424);
            this.panel5.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gridControlQLTT_KhachHang);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(472, 424);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách khách hàng";
            // 
            // gridControlQLTT_KhachHang
            // 
            this.gridControlQLTT_KhachHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlQLTT_KhachHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlQLTT_KhachHang.Location = new System.Drawing.Point(3, 17);
            this.gridControlQLTT_KhachHang.MainView = this.gridView1;
            this.gridControlQLTT_KhachHang.Name = "gridControlQLTT_KhachHang";
            this.gridControlQLTT_KhachHang.Size = new System.Drawing.Size(466, 404);
            this.gridControlQLTT_KhachHang.TabIndex = 1;
            this.gridControlQLTT_KhachHang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.CustomerID,
            this.CustomerTypeName,
            this.FirstName,
            this.LastName,
            this.IDNumber,
            this.Gender,
            this.Address,
            this.Phone,
            this.Phone1,
            this.Email,
            this.RegisterDate});
            this.gridView1.GridControl = this.gridControlQLTT_KhachHang;
            this.gridView1.Name = "gridView1";
            this.gridView1.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // CustomerID
            // 
            this.CustomerID.Caption = "Mã KH";
            this.CustomerID.FieldName = "CustomerID";
            this.CustomerID.Name = "CustomerID";
            this.CustomerID.Visible = true;
            this.CustomerID.VisibleIndex = 0;
            // 
            // CustomerTypeName
            // 
            this.CustomerTypeName.Caption = "Loại KH";
            this.CustomerTypeName.FieldName = "CustomerTypeName";
            this.CustomerTypeName.Name = "CustomerTypeName";
            this.CustomerTypeName.Visible = true;
            this.CustomerTypeName.VisibleIndex = 1;
            // 
            // FirstName
            // 
            this.FirstName.Caption = "Họ";
            this.FirstName.FieldName = "FirstName";
            this.FirstName.Name = "FirstName";
            this.FirstName.Visible = true;
            this.FirstName.VisibleIndex = 2;
            // 
            // LastName
            // 
            this.LastName.Caption = "Tên";
            this.LastName.FieldName = "LastName";
            this.LastName.Name = "LastName";
            this.LastName.Visible = true;
            this.LastName.VisibleIndex = 3;
            // 
            // IDNumber
            // 
            this.IDNumber.Caption = "CMND";
            this.IDNumber.FieldName = "IDNumber";
            this.IDNumber.Name = "IDNumber";
            this.IDNumber.Visible = true;
            this.IDNumber.VisibleIndex = 4;
            // 
            // Gender
            // 
            this.Gender.Caption = "Giới tính";
            this.Gender.FieldName = "Gender";
            this.Gender.Name = "Gender";
            this.Gender.Visible = true;
            this.Gender.VisibleIndex = 10;
            // 
            // Address
            // 
            this.Address.Caption = "Địa chỉ";
            this.Address.FieldName = "Address";
            this.Address.Name = "Address";
            this.Address.Visible = true;
            this.Address.VisibleIndex = 5;
            // 
            // Phone
            // 
            this.Phone.Caption = "SĐT";
            this.Phone.FieldName = "Phone";
            this.Phone.Name = "Phone";
            this.Phone.Visible = true;
            this.Phone.VisibleIndex = 6;
            // 
            // Phone1
            // 
            this.Phone1.Caption = "SĐT khác";
            this.Phone1.FieldName = "Phone1";
            this.Phone1.Name = "Phone1";
            this.Phone1.Visible = true;
            this.Phone1.VisibleIndex = 7;
            // 
            // Email
            // 
            this.Email.Caption = "Email";
            this.Email.FieldName = "Email";
            this.Email.Name = "Email";
            this.Email.Visible = true;
            this.Email.VisibleIndex = 8;
            // 
            // RegisterDate
            // 
            this.RegisterDate.Caption = "Ngày ĐK";
            this.RegisterDate.FieldName = "RegisterDate";
            this.RegisterDate.Name = "RegisterDate";
            this.RegisterDate.Visible = true;
            this.RegisterDate.VisibleIndex = 9;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(708, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(236, 424);
            this.panel2.TabIndex = 4;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tabControl1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(236, 424);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Thông tin khách hàng";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 17);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(230, 404);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblCusID);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.xtraUserControl1);
            this.tabPage1.Controls.Add(this.lblCustomerType);
            this.tabPage1.Controls.Add(this.lblCustomerID);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(222, 378);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin chung";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // xtraUserControl1
            // 
            this.xtraUserControl1.Location = new System.Drawing.Point(11, 25);
            this.xtraUserControl1.Name = "xtraUserControl1";
            this.xtraUserControl1.Size = new System.Drawing.Size(150, 150);
            this.xtraUserControl1.TabIndex = 44;
            // 
            // lblCustomerType
            // 
            this.lblCustomerType.AutoSize = true;
            this.lblCustomerType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerType.ForeColor = System.Drawing.Color.Blue;
            this.lblCustomerType.Location = new System.Drawing.Point(55, 236);
            this.lblCustomerType.Name = "lblCustomerType";
            this.lblCustomerType.Size = new System.Drawing.Size(42, 20);
            this.lblCustomerType.TabIndex = 43;
            this.lblCustomerType.Text = "label";
            // 
            // lblCustomerID
            // 
            this.lblCustomerID.AutoSize = true;
            this.lblCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerID.ForeColor = System.Drawing.Color.Blue;
            this.lblCustomerID.Location = new System.Drawing.Point(55, 88);
            this.lblCustomerID.Name = "lblCustomerID";
            this.lblCustomerID.Size = new System.Drawing.Size(42, 20);
            this.lblCustomerID.TabIndex = 42;
            this.lblCustomerID.Text = "label";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 20);
            this.label3.TabIndex = 41;
            this.label3.Text = "2. Loại KH:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 40;
            this.label2.Text = "1. Mã KH:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblGender);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.lblRegisterDate);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.lblIDNumber);
            this.tabPage2.Controls.Add(this.lblLastName);
            this.tabPage2.Controls.Add(this.lblFirstName);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(222, 378);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Thông tin chi tiết";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.ForeColor = System.Drawing.Color.Blue;
            this.lblGender.Location = new System.Drawing.Point(36, 257);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(42, 20);
            this.lblGender.TabIndex = 49;
            this.lblGender.Text = "label";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(16, 222);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 20);
            this.label12.TabIndex = 48;
            this.label12.Text = "3.Giới Tính:";
            // 
            // lblRegisterDate
            // 
            this.lblRegisterDate.AutoSize = true;
            this.lblRegisterDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterDate.ForeColor = System.Drawing.Color.Blue;
            this.lblRegisterDate.Location = new System.Drawing.Point(36, 343);
            this.lblRegisterDate.Name = "lblRegisterDate";
            this.lblRegisterDate.Size = new System.Drawing.Size(42, 20);
            this.lblRegisterDate.TabIndex = 47;
            this.lblRegisterDate.Text = "label";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(16, 308);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 20);
            this.label11.TabIndex = 46;
            this.label11.Text = "4.Ngày ĐK:";
            // 
            // lblIDNumber
            // 
            this.lblIDNumber.AutoSize = true;
            this.lblIDNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIDNumber.ForeColor = System.Drawing.Color.Blue;
            this.lblIDNumber.Location = new System.Drawing.Point(36, 183);
            this.lblIDNumber.Name = "lblIDNumber";
            this.lblIDNumber.Size = new System.Drawing.Size(42, 20);
            this.lblIDNumber.TabIndex = 43;
            this.lblIDNumber.Text = "label";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.ForeColor = System.Drawing.Color.Blue;
            this.lblLastName.Location = new System.Drawing.Point(40, 107);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(42, 20);
            this.lblLastName.TabIndex = 44;
            this.lblLastName.Text = "label";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstName.ForeColor = System.Drawing.Color.Blue;
            this.lblFirstName.Location = new System.Drawing.Point(38, 44);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(42, 20);
            this.lblFirstName.TabIndex = 45;
            this.lblFirstName.Text = "label";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 20);
            this.label6.TabIndex = 40;
            this.label6.Text = "3. CMND:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 20);
            this.label5.TabIndex = 41;
            this.label5.Text = "2. Tên:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 20);
            this.label4.TabIndex = 42;
            this.label4.Text = "1. Họ:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lblEmail);
            this.tabPage3.Controls.Add(this.lblPhone1);
            this.tabPage3.Controls.Add(this.lblPhone);
            this.tabPage3.Controls.Add(this.lblAddress);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.ForeColor = System.Drawing.Color.Black;
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(222, 378);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Liên hệ";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.ForeColor = System.Drawing.Color.Blue;
            this.lblEmail.Location = new System.Drawing.Point(58, 319);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(42, 20);
            this.lblEmail.TabIndex = 51;
            this.lblEmail.Text = "label";
            // 
            // lblPhone1
            // 
            this.lblPhone1.AutoSize = true;
            this.lblPhone1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone1.ForeColor = System.Drawing.Color.Blue;
            this.lblPhone1.Location = new System.Drawing.Point(52, 235);
            this.lblPhone1.Name = "lblPhone1";
            this.lblPhone1.Size = new System.Drawing.Size(42, 20);
            this.lblPhone1.TabIndex = 50;
            this.lblPhone1.Text = "label";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.ForeColor = System.Drawing.Color.Blue;
            this.lblPhone.Location = new System.Drawing.Point(52, 149);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(42, 20);
            this.lblPhone.TabIndex = 49;
            this.lblPhone.Text = "label";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.ForeColor = System.Drawing.Color.Blue;
            this.lblAddress.Location = new System.Drawing.Point(52, 73);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(42, 20);
            this.lblAddress.TabIndex = 48;
            this.lblAddress.Text = "label";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(17, 283);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 20);
            this.label10.TabIndex = 44;
            this.label10.Text = "4. Email:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(17, 195);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 20);
            this.label9.TabIndex = 45;
            this.label9.Text = "3. SĐT khác:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(17, 116);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 20);
            this.label8.TabIndex = 47;
            this.label8.Text = "2. SĐT:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(17, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 20);
            this.label7.TabIndex = 46;
            this.label7.Text = "1. Địa Chỉ:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(236, 424);
            this.panel1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gridControl1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(236, 424);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin loại KH";
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(3, 17);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(230, 404);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.CustomerTypeID,
            this.CusTypeName,
            this.DiscountRatio});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.Click += new System.EventHandler(this.gridView2_Click);
            // 
            // CustomerTypeID
            // 
            this.CustomerTypeID.Caption = "Mã Loại KH";
            this.CustomerTypeID.FieldName = "CustomerTypeID";
            this.CustomerTypeID.Name = "CustomerTypeID";
            this.CustomerTypeID.Visible = true;
            this.CustomerTypeID.VisibleIndex = 0;
            // 
            // CusTypeName
            // 
            this.CusTypeName.Caption = "Loại KH";
            this.CusTypeName.FieldName = "CustomerTypeName";
            this.CusTypeName.Name = "CusTypeName";
            this.CusTypeName.Visible = true;
            this.CusTypeName.VisibleIndex = 1;
            // 
            // DiscountRatio
            // 
            this.DiscountRatio.Caption = "Hệ số giảm giá";
            this.DiscountRatio.FieldName = "DiscountRatio";
            this.DiscountRatio.Name = "DiscountRatio";
            this.DiscountRatio.Visible = true;
            this.DiscountRatio.VisibleIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnReload);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.btnPrintCus);
            this.panel4.Controls.Add(this.btnUpdateCus);
            this.panel4.Controls.Add(this.btnAddCus);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.btnDelCus);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.btnUpdateCusType);
            this.panel4.Controls.Add(this.btnDelCusType);
            this.panel4.Controls.Add(this.btnAddCusType);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 424);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(944, 61);
            this.panel4.TabIndex = 2;
            // 
            // btnReload
            // 
            this.btnReload.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReload.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnReload.Appearance.Options.UseBackColor = true;
            this.btnReload.Appearance.Options.UseFont = true;
            this.btnReload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnReload.Image = ((System.Drawing.Image)(resources.GetObject("btnReload.Image")));
            this.btnReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnReload.Location = new System.Drawing.Point(873, 1);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(68, 57);
            this.btnReload.TabIndex = 22;
            this.btnReload.Text = "Tải lại";
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(866, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1, 50);
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            // 
            // btnPrintCus
            // 
            this.btnPrintCus.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPrintCus.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnPrintCus.Appearance.Options.UseBackColor = true;
            this.btnPrintCus.Appearance.Options.UseFont = true;
            this.btnPrintCus.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnPrintCus.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintCus.Image")));
            this.btnPrintCus.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnPrintCus.Location = new System.Drawing.Point(718, 3);
            this.btnPrintCus.Name = "btnPrintCus";
            this.btnPrintCus.Size = new System.Drawing.Size(142, 57);
            this.btnPrintCus.TabIndex = 20;
            this.btnPrintCus.Text = "In Danh Sách KH";
            // 
            // btnUpdateCus
            // 
            this.btnUpdateCus.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnUpdateCus.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnUpdateCus.Appearance.Options.UseBackColor = true;
            this.btnUpdateCus.Appearance.Options.UseFont = true;
            this.btnUpdateCus.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnUpdateCus.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateCus.Image")));
            this.btnUpdateCus.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnUpdateCus.Location = new System.Drawing.Point(437, 0);
            this.btnUpdateCus.Name = "btnUpdateCus";
            this.btnUpdateCus.Size = new System.Drawing.Size(88, 57);
            this.btnUpdateCus.TabIndex = 14;
            this.btnUpdateCus.Text = "Sửa KH";
            this.btnUpdateCus.Click += new System.EventHandler(this.btnUpdateCus_Click);
            // 
            // btnAddCus
            // 
            this.btnAddCus.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAddCus.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddCus.Appearance.Options.UseBackColor = true;
            this.btnAddCus.Appearance.Options.UseFont = true;
            this.btnAddCus.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddCus.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCus.Image")));
            this.btnAddCus.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnAddCus.Location = new System.Drawing.Point(332, 0);
            this.btnAddCus.Name = "btnAddCus";
            this.btnAddCus.Size = new System.Drawing.Size(99, 57);
            this.btnAddCus.TabIndex = 1;
            this.btnAddCus.Text = "Thêm KH";
            this.btnAddCus.Click += new System.EventHandler(this.btnAddCus_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(707, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1, 50);
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // btnDelCus
            // 
            this.btnDelCus.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDelCus.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDelCus.Appearance.Options.UseBackColor = true;
            this.btnDelCus.Appearance.Options.UseFont = true;
            this.btnDelCus.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnDelCus.Image = ((System.Drawing.Image)(resources.GetObject("btnDelCus.Image")));
            this.btnDelCus.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDelCus.Location = new System.Drawing.Point(538, 1);
            this.btnDelCus.Name = "btnDelCus";
            this.btnDelCus.Size = new System.Drawing.Size(91, 57);
            this.btnDelCus.TabIndex = 13;
            this.btnDelCus.Text = "Xóa KH";
            this.btnDelCus.Click += new System.EventHandler(this.btnDelCus_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(318, 8);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1, 50);
            this.pictureBox3.TabIndex = 18;
            this.pictureBox3.TabStop = false;
            // 
            // btnUpdateCusType
            // 
            this.btnUpdateCusType.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnUpdateCusType.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnUpdateCusType.Appearance.Options.UseBackColor = true;
            this.btnUpdateCusType.Appearance.Options.UseFont = true;
            this.btnUpdateCusType.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnUpdateCusType.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateCusType.Image")));
            this.btnUpdateCusType.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnUpdateCusType.Location = new System.Drawing.Point(101, 1);
            this.btnUpdateCusType.Name = "btnUpdateCusType";
            this.btnUpdateCusType.Size = new System.Drawing.Size(108, 57);
            this.btnUpdateCusType.TabIndex = 17;
            this.btnUpdateCusType.Text = "Sửa loại KH";
            this.btnUpdateCusType.Click += new System.EventHandler(this.btnUpdateCusType_Click);
            // 
            // btnDelCusType
            // 
            this.btnDelCusType.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDelCusType.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDelCusType.Appearance.Options.UseBackColor = true;
            this.btnDelCusType.Appearance.Options.UseFont = true;
            this.btnDelCusType.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnDelCusType.Image = ((System.Drawing.Image)(resources.GetObject("btnDelCusType.Image")));
            this.btnDelCusType.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDelCusType.Location = new System.Drawing.Point(215, 4);
            this.btnDelCusType.Name = "btnDelCusType";
            this.btnDelCusType.Size = new System.Drawing.Size(87, 57);
            this.btnDelCusType.TabIndex = 16;
            this.btnDelCusType.Text = "Xóa loại KH";
            this.btnDelCusType.Click += new System.EventHandler(this.btnDelCusType_Click);
            // 
            // btnAddCusType
            // 
            this.btnAddCusType.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAddCusType.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddCusType.Appearance.Options.UseBackColor = true;
            this.btnAddCusType.Appearance.Options.UseFont = true;
            this.btnAddCusType.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddCusType.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCusType.Image")));
            this.btnAddCusType.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnAddCusType.Location = new System.Drawing.Point(3, 1);
            this.btnAddCusType.Name = "btnAddCusType";
            this.btnAddCusType.Size = new System.Drawing.Size(92, 57);
            this.btnAddCusType.TabIndex = 15;
            this.btnAddCusType.Text = "Thêm Loại KH";
            this.btnAddCusType.Click += new System.EventHandler(this.btnAddCusType_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 20);
            this.label1.TabIndex = 45;
            this.label1.Text = "1. Mã KH:";
            // 
            // lblCusID
            // 
            this.lblCusID.AutoSize = true;
            this.lblCusID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCusID.ForeColor = System.Drawing.Color.Blue;
            this.lblCusID.Location = new System.Drawing.Point(55, 108);
            this.lblCusID.Name = "lblCusID";
            this.lblCusID.Size = new System.Drawing.Size(42, 20);
            this.lblCusID.TabIndex = 46;
            this.lblCusID.Text = "label";
            // 
            // frmQLTT_KhachHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 485);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmQLTT_KhachHang";
            this.Text = "frmQLTT_KhachHang";
            this.Load += new System.EventHandler(this.frmQLTT_KhachHang_Load);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlQLTT_KhachHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.SimpleButton btnReload;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.SimpleButton btnPrintCus;
        private DevExpress.XtraEditors.SimpleButton btnUpdateCus;
        private DevExpress.XtraEditors.SimpleButton btnAddCus;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton btnDelCus;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraEditors.SimpleButton btnUpdateCusType;
        private DevExpress.XtraEditors.SimpleButton btnDelCusType;
        private DevExpress.XtraEditors.SimpleButton btnAddCusType;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraGrid.GridControl gridControlQLTT_KhachHang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn CustomerID;
        private DevExpress.XtraGrid.Columns.GridColumn CustomerTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn FirstName;
        private DevExpress.XtraGrid.Columns.GridColumn LastName;
        private DevExpress.XtraGrid.Columns.GridColumn IDNumber;
        private DevExpress.XtraGrid.Columns.GridColumn Address;
        private DevExpress.XtraGrid.Columns.GridColumn Phone;
        private DevExpress.XtraGrid.Columns.GridColumn Phone1;
        private DevExpress.XtraGrid.Columns.GridColumn Email;
        private DevExpress.XtraGrid.Columns.GridColumn RegisterDate;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn CustomerTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn CusTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn DiscountRatio;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lblCustomerType;
        private System.Windows.Forms.Label lblCustomerID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lblIDNumber;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPhone1;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblRegisterDate;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.XtraUserControl xtraUserControl1;
        private DevExpress.XtraGrid.Columns.GridColumn Gender;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblCusID;
        private System.Windows.Forms.Label label1;

    }
}