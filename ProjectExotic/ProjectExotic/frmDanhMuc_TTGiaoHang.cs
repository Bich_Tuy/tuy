﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticDTO;
using ExoticDAL;
using ExoticBLL;


namespace ProjectExotic
{
    public partial class frmDanhMuc_TTGiaoHang : DevExpress.XtraEditors.XtraForm
    {
        ShopCommerceDataContext dbs = null;
        bllShipments bll = null;
        dtoShipments dto=null;
        void loadData()
        {
            bll = new bllShipments();
            gridControl1.DataSource = bll.getTable();
        }
        void bindingData()
        {
            try
            {
                lblShipmentID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["ShipmentID"]).ToString();
                lblExportBillID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["ExportBillID"]).ToString();
                lblCustomerName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["CustomerName"]).ToString();
                lblEmployeeName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["EmployeeName"]).ToString();
                lblAddress.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Address"]).ToString();
                lblPhone.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone"]).ToString();
                lblShippingPrice.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["ShippingPrices"]).ToString();
                lblTime.Text =Convert.ToDateTime( gridView1.GetFocusedRowCellValue(gridView1.Columns["Time"])).ToString("dd/MM/yyyy");
                // neu truong hop phone 1 null
                if (gridView1.Columns["Phone1"]==null)
                {
                    lblPhone1.Text = " ";
                }
                else
                {
                    lblPhone1.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone1"]).ToString();
                }
                
            }
            catch (NullReferenceException)
            { }
            catch (InvalidCastException)
            { }
        }
        public frmDanhMuc_TTGiaoHang()
        {
            InitializeComponent();
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            bindingData();
        }

        private void gridControl1_Load(object sender, EventArgs e)
        {

        }

        private void frmDanhMuc_TTGiaoHang_Load(object sender, EventArgs e)
        {
            loadData();
            gridView1_Click(null, null);
        }
    }
}