﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticBLL;
using ExoticDAL;
using ExoticDTO;
using System.Linq;

namespace ProjectExotic
{
    public partial class frmThemCapNhat_KH : DevExpress.XtraEditors.XtraForm
    {
        #region các thuộc tính và hàm
        public string customerID;// thuộc tính dùng để truyền vào form chính
        ShopCommerceDataContext dbs = null;
        dtoCustomers dto=null;
        bllCustomers bll=null;
        bool them = frmQLTT_KhachHang.them;
        // đặt trạng thái form khi thêm/ cập nhật dữ liệu
        
        void setPanel()
        {
            if(them==true)
            {
                txtCustomerID.Enabled = true;
            }
            else
            {
                txtCustomerID.Enabled = false;
                dbs = new ShopCommerceDataContext();
                // truyền dữ liệu
                customerID = frmQLTT_KhachHang.customerID;
                // tạo đối tượng
                Customer data = (from n in dbs.Customers
                                 where n.CustomerID == frmQLTT_KhachHang.customerID
                                 select n).First();
                // binding dữ liệu
                txtCustomerID.Text = data.CustomerID;
                comboBox1.Text = data.CustomerType.CustomerTypeName;
                txtLastName.Text = data.FirstName;
                txtFirstName.Text = data.LastName;
                txtNumberID.Text = data.IDNumber;
                txtAddress.Text = data.Address;
                txtPhone.Text = data.Phone;
                txtPhone1.Text = data.Phone1;
                txtEmail.Text = data.Email;
                Datetime.Value =Convert.ToDateTime(data.RegisterDate);
            }
        }
        void addData()
        {
            dto = new dtoCustomers();
            bll = new bllCustomers();
            string err = "";
            // nạp dữ liệu
            dto.CustomerID = txtCustomerID.Text.Trim();
            dto.CustomerTypeID = comboBox1.SelectedValue.ToString();
            dto.FirstName = txtFirstName.Text.Trim();
            dto.LastName = txtLastName.Text.Trim();
            dto.Email = txtEmail.Text.Trim();
            dto.IDNumber = txtNumberID.Text.Trim();
            dto.Phone = txtPhone.Text.Trim();
            dto.Phone1 = txtPhone1.Text.Trim();
            dto.RegisterDate = Datetime.Value.Date;
            dto.Address = txtAddress.Text.Trim();
            if (radioButtonNam.Checked == true)
            {
                dto.Gender = false;
            }
            else
                dto.Gender = true;
            
            bool f = bll.insertData(dto, ref err);
            if (f==true)
            {
                MessageBox.Show("Thêm dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                customerID=dto.CustomerID;
                this.Close();
            }
            else
            {
                MessageBox.Show("Thêm dữ liệu thất bại"+err, "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //txtLastName.Focus();
                //txtFirstName.Focus();
            }
        }
        // cập nhật dữ liệu
        void updateData()
        {
            dto = new dtoCustomers();
            bll = new bllCustomers();
            string err = "";
            // nạp dữ liệu
            dto.CustomerID = txtCustomerID.Text.Trim();
            dto.CustomerTypeID = comboBox1.SelectedValue.ToString();
            dto.FirstName = txtFirstName.Text.Trim();
            dto.LastName = txtLastName.Text.Trim();
            dto.Email = txtEmail.Text.Trim();
            dto.IDNumber = txtNumberID.Text.Trim();
            dto.Address = txtAddress.Text.Trim();
            dto.Phone = txtPhone.Text.Trim();
            dto.Phone1 = txtPhone1.Text.Trim();
            dto.RegisterDate = Datetime.Value.Date;
            if (radioButtonNam.Checked == true)
            {
                dto.Gender = false;
            }
            else
                dto.Gender = true;
            
            bool f = bll.updateData(dto, ref err);
            if (f)
            {
                MessageBox.Show("cập nhật dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                customerID = dto.CustomerID;
                this.Close();
            }
            else
            {
                MessageBox.Show("cập nhật dữ liệu thất bại" +err, "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtLastName.Focus();
                txtFirstName.Focus();
            }
        }

        void checkData()
        {
            bool right = true;//Biến dùng để kiểm tra dữ liệu
            if(txtCustomerID.Text.Length>10)
            {
                right = false;
                MessageBox.Show("Độ dài của mã khách hàng không được quá 10 kí tự.  ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtLastName.Text.Length>50)
            {
                right = false;
                MessageBox.Show("Độ dài của họ và tên lót không được quá 50 kí tự. ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtFirstName.Text.Length>20)
            {
                right = false;
                MessageBox.Show("Độ dài của tên không được quá 20 kí tự.", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtAddress.Text.Length>50)
            {
                right = false;
                MessageBox.Show("Độ dài của địa chỉ không được quá 50 kí tự. ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtEmail.Text.Length>30)
            {
                right = false;
                MessageBox.Show("Độ dài của email không được quá 30 kí tự. ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
            if(txtNumberID.Text.Length>10)
            {
                right = false;
                MessageBox.Show("Độ dài của số chứng minh nhân dân không được quá 10 kí tự. ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtPhone.Text.Length>15)
            {
                right = false;
                MessageBox.Show("Độ dài của số điện thoại không được quá 15 kí tự. ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (txtPhone1.Text.Length > 15)
            {
                right = false;
                MessageBox.Show("Độ dài của số điện thoại không được quá 15 kí tự. ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //if (txtEmail.Text != @"\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b")
            //{
            //    right = false;
            //    MessageBox.Show("định dạng email sai rồi ", "Lỗi",
            //        MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            if (right)
            {
                if (them)
                {
                    addData();
                }
                else
                {
                    updateData();
                }
            }

        }
        
        #endregion

        #region các sự kiện và phương thức

        public frmThemCapNhat_KH()
        {
            InitializeComponent();
        }

       

        private void btnHuy_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Có thể bạn chưa lưu dữ liệu.\nBạn có thật sự muốn thoát hay không?", "Thoát",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialog == DialogResult.OK)
            {
                this.Dispose();
                this.Close();
            }
            else
            {
                txtCustomerID.Focus();
            }

        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            checkData();
        }

        private void frmThemCapNhat_KH_Load(object sender, EventArgs e)
        {
            dbs = new ShopCommerceDataContext();
            // đổ dữ liệu vào combobox
            var data = from n in dbs.CustomerTypes
                       select n;
            comboBox1.DataSource = data;
            comboBox1.DisplayMember = "CustomerTypeName";
            comboBox1.ValueMember = "CustomerTypeID";
            if (this.Text == "Thêm Khách Hàng")
            {
                them = true;
            }
            else
            {
                them = false;
                setPanel();
            }

        }
        

        private void txtCustomerID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar==13)
            {
                comboBox1.Focus();
            }
        }

        private void comboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtFirstName.Focus();
            }
        }

        private void txtLastname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtLastName.Focus();
            }
        }

        private void txtFirstname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtNumberID.Focus();
            }
        }

        private void txtNumberID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtAddress.Focus();
            }
        }

        private void txtAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtPhone.Focus();
            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtPhone1.Focus();
            }
        }

        private void txtPhone1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtEmail.Focus();
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                Datetime.Focus();
            }
        }



    }
        #endregion
}