﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ProjectExotic
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        public MainForm()
        {
            InitializeComponent();
        }

        static int KiemTraTonTai(DevExpress.XtraTab.XtraTabControl tabControlName, string tabName)
        {
            int re = -1;
            for (int i = 0; i < tabControlName.TabPages.Count; i++)
            {
                if (tabControlName.TabPages[i].Name == tabName)
                {
                    re = i;
                    break;
                }
            }
            return re;
        }
        /// <summary>
        /// Tạo thêm tab mới
        /// </summary>
        /// <param name="tabControl">Tên TabControl để add thêm tabpage mới vào</param>
        /// <param name="Text">Tiêu đề tabpage mới</param>
        /// <param name="Name">Tên tabpage mới</param>
        /// <param name="form">Tên form con của tab mới</param>
        /// <param name="imageIndex">index của icon</param>
        void TabCreating(DevExpress.XtraTab.XtraTabControl tabControl, string Text, string Name, DevExpress.XtraEditors.XtraForm form, int imageIndex)
        {
            int index = KiemTraTonTai(tabControl, Name);
            if (index >= 0)
            {
                tabControl.SelectedTabPage = tabControl.TabPages[index];
                tabControl.SelectedTabPage.Text = Text;
            }
            else
            {
                DevExpress.XtraTab.XtraTabPage tabpage = new DevExpress.XtraTab.XtraTabPage { Text = Text, Name = Name, ImageIndex = imageIndex };
                tabControl.TabPages.Add(tabpage);
                tabControl.SelectedTabPage = tabpage;

                form.TopLevel = false;
                form.Parent = tabpage;
                form.Show();
                form.Dock = DockStyle.Fill;
            }
        }
        private void tileBar1_Click(object sender, EventArgs e)
        {

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.ShowDialog();
            this.WindowState = FormWindowState.Maximized;
            btnQLTT_ThuongHieu_Click(null, null);
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            try
            {
                string pageName = xtraTabControl1.SelectedTabPage.Text;
                this.Text = pageName + " | Phần mềm quản lý kinh doanh thời trang iFashion version 2.0";
                if (pageName.Contains("Hoạt Động"))
                {
                    btnDangXuat.Appearance.BackColor = Color.FromArgb(33, 150, 243);
                    panelFooter.BackColor = Color.FromArgb(33, 150, 243);
                }
                if (pageName.Contains("Danh Mục"))
                {
                    panelFooter.BackColor = Color.FromArgb(0, 150, 136);
                    btnDangXuat.Appearance.BackColor = Color.FromArgb(0, 150, 136);
                }
                if (pageName.Contains("QLNH") || pageName.Contains("QLXH") || pageName.Contains("QLTT"))
                {
                    panelFooter.BackColor = Color.FromArgb(102, 102, 102);
                    btnDangXuat.Appearance.BackColor = Color.FromArgb(102, 102, 102);
                }
                if (pageName.Contains("Thống Kê"))
                {
                    panelFooter.BackColor = Color.FromArgb(216, 67, 21);
                    btnDangXuat.Appearance.BackColor = Color.FromArgb(216, 67, 21);
                }
                if (pageName.Contains("Hệ Thống"))
                {
                    btnDangXuat.BackColor = Color.FromArgb(76, 175, 80);
                    btnDangXuat.Appearance.BackColor = Color.FromArgb(76, 175, 80);
                }
                if (pageName.Contains("Hỗ Trợ"))
                {
                    btnDangXuat.BackColor = Color.FromArgb(3, 169, 244);
                    btnDangXuat.Appearance.BackColor = Color.FromArgb(3, 169, 244);
                }
            }
            catch(NullReferenceException)
            { }
        }

        private void btnHoatDong_NhapHang_Click(object sender, EventArgs e)
        {
            FrmHoatDong_NhapHang frm = new FrmHoatDong_NhapHang();

            TabCreating(xtraTabControl1, "Hoạt Động > Nhập Hàng", "Hoạt Động Nhập Hàng", frm, -1);
            tileBar1.HideDropDownWindow();
        }

        private void btnDanhMuc_MatHang_Click(object sender, EventArgs e)
        {
            FrmDanhMuc_MatHang frm = new FrmDanhMuc_MatHang();

            TabCreating(xtraTabControl1, "Danh Mục > Mặt Hàng", "Danh Mục Mặt Hàng", frm, -1);
            tileBar1.HideDropDownWindow();
        }

        private void btnHoatDong_ItemClick(object sender, TileItemEventArgs e)
        {
        }

        private void xtraTabControl1_CloseButtonClick(object sender, EventArgs e)
        {
            DevExpress.XtraTab.XtraTabControl xtab = (DevExpress.XtraTab.XtraTabControl)sender;
            if (xtab.SelectedTabPageIndex == 0) return;
            int i = xtab.SelectedTabPageIndex;
            xtab.TabPages.RemoveAt(xtab.SelectedTabPageIndex);
            xtab.SelectedTabPageIndex = i - 1;
        }

        private void btnQLTT_ThuongHieu_Click(object sender, EventArgs e)
        {
            FrmQLTT_ThuongHieu frm = new FrmQLTT_ThuongHieu();
            TabCreating(xtraTabControl1, "QLTT > Thương Hiệu", "QLTT Thương Hiệu", frm, -1);
            tileBar1.HideDropDownWindow();
        }
        //Đăng xuất
        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Bạn có thật sự muốn đăng xuất hay không? \n Có thể bạn chưa lưu công việc.",
               "Cảnh báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialog == DialogResult.OK)
            {
                xtraTabControl1.TabPages.Clear();
                this.Hide();
                FrmLogin frm = new FrmLogin();
                frm.ShowDialog();
                //try
                //{
                //    if (FrmLogin.MaNV == "admi")
                //    {
                //        ribbonPage3.Visible = true;
                //        // ribbonPage4.Visible = true;
                //    }
                //    else
                //    {
                //        ribbonPage3.Visible = false;
                //        //ribbonPage4.Visible = false;
                //    }
                //    barStaticItem1.Caption = ChuanHoaChuoi.StandardString("Nhân viên đăng nhập : " + FrmLogin.tenNV) + " | Mã số : " + FrmLogin.MaNV.Trim();
                //}
                //catch (NullReferenceException)
                //{ }
                this.Show();
            }
        }

        private void btnQLTT_NhanVien_Click(object sender, EventArgs e)
        {
            frmQLTT_NhanVien frm = new frmQLTT_NhanVien();
            TabCreating(xtraTabControl1, "QLTT > Nhân Viên", "QLTT Nhân Viên", frm, -1);
            tileBar1.HideDropDownWindow();
        }

        private void btnQLTT_MatHang_Click(object sender, EventArgs e)
        {
            FrmQLTT_MatHang frm = new FrmQLTT_MatHang();
            TabCreating(xtraTabControl1, "QLTT > Mặt Hàng", "QLTT Mặt Hàng", frm, -1);
            tileBar1.HideDropDownWindow();
        }

        private void btnQLXH_ThongTinGiaoHang_Click(object sender, EventArgs e)
        {
            frmQLXH_TTGiaoHang frm = new frmQLXH_TTGiaoHang();
            TabCreating(xtraTabControl1, "QLXH > Thông Tin Giao Hàng", "QLXH Thông Tin Giao Hàng", frm, -1);
            tileBar1.HideDropDownWindow();
        }

        private void btnDanhMuc_KhachHang_Click(object sender, EventArgs e)
        {
            frmDanhMuc_KhachHang frm = new frmDanhMuc_KhachHang();
            TabCreating(xtraTabControl1, "Danh Mục > Khách Hàng", "Danh Mục Khách Hàng", frm, -1);
            tileBar1.HideDropDownWindow();
        }

        private void btnDanhMuc_ThongTinGiaoHang_Click(object sender, EventArgs e)
        {
            frmDanhMuc_TTGiaoHang frm = new frmDanhMuc_TTGiaoHang();
            TabCreating(xtraTabControl1, "Danh Mục > Thông Tin Giao Hàng", "Danh Mục Thông Tin Giao Hàng", frm, -1);
            tileBar1.HideDropDownWindow();
        }

        private void btnHoatDong_GiaoHang_Click(object sender, EventArgs e)
        {
            frmHoatDong_GiaoHang frm = new frmHoatDong_GiaoHang();
            TabCreating(xtraTabControl1, "Hoạt Động > Giao Hàng", "Hoạt Động Giao Hàng", frm, -1);
            tileBar1.HideDropDownWindow();
        }

        private void btnQLTT_KhachHang_Click(object sender, EventArgs e)
        {
            frmQLTT_KhachHang frm = new frmQLTT_KhachHang();
            TabCreating(xtraTabControl1, "QLTT > Khách Hàng", "QLTT Khách Hàng", frm, -1);
            tileBar1.HideDropDownWindow();
        }
    }
}