﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticBLL;
using ExoticDAL;
using ExoticDTO;
using System.IO;
using System.Drawing;


namespace ProjectExotic
{
    public partial class frmQLTT_NhanVien : DevExpress.XtraEditors.XtraForm
    {
        ShopCommerceDataContext dbs = null;
        #region các biến -hàm
        bllEmployees bllEm = null;// gọi lớp bll
        dtoEmployees dtoEm = null;

        bllEmployeeTypes bllEmType = null;
        dtoEmployeeTypes dtoEmType = null;
        
        public static string employeeID;// biến dùng khi cập nhật nhân viên
        public static string employeeTypeID;// biến dùng khi cập nhật loại nhân viên
        public static bool them;/// cập nhật hay thêm
                                /// button thêm thì them=true
                                /// button cập nhật thì them=false
                                /// 
        public Image byteArrayToImage(byte[] arrImage)
        {

            MemoryStream ms = new MemoryStream(arrImage);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        void focusInARow(string var)
        {
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (var == gridView1.GetRowCellValue(i, gridView1.Columns["EmployeeID"]).ToString())
                {
                    gridView1.FocusedRowHandle = i;
                    break;
                }
            }
            for (int r = 0; r < gridView2.RowCount; r++)
            {
                if (var == gridView2.GetRowCellValue(r, gridView2.Columns["EmployeeTypeID"]).ToString())
                {
                    gridView2.FocusedRowHandle = r;
                    break;
                }
            }
        }
        // load dữ liệu lên gridcontrol
        void loadData()
        {
            bllEmType = new bllEmployeeTypes();
            gridControl1.DataSource = bllEmType.getTable();
            bllEm = new bllEmployees();
            gridControl2.DataSource = bllEm.getTable();
        }
        //chuyển đổi dữ liệu dạng nhị phân sang Image
        public Image byteToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        void bindingData()
        {
            try
            {
                lblEmployeeID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["EmployeeID"]).ToString();
                lblEmployeeType.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["EmployeeTypeName"]).ToString();
                string firstName = gridView1.GetFocusedRowCellValue(gridView1.Columns["FirstName"]).ToString();
                string lastName = gridView1.GetFocusedRowCellValue(gridView1.Columns["LastName"]).ToString();
                lblName.Text = (firstName.Trim() + " " + lastName.Trim()).Trim();
                lblBirthday.Text =Convert.ToDateTime( gridView1.GetFocusedRowCellValue(gridView1.Columns["Birthday"])).ToString("dd/MM/yyyy");
                lblHireday.Text =Convert.ToDateTime( gridView1.GetFocusedRowCellValue(gridView1.Columns["HireDay"])).ToString("dd/MM/yyyy");
                lblIDNumber.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["IDNumber"]).ToString();
                lblGender.Text = Convert.ToBoolean(gridView1.GetFocusedRowCellValue(gridView1.Columns["Gender"])) == true ? "Nữ" : "Nam";
                lblAddress.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Address"]).ToString();
                lblPhone.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone"]).ToString();
                //Employee nhanvien=(from n in dbs.Employees
                //                       where n.EmployeeID==lblEmployeeID.Text
                //                       select n).First();
                //byte[] arrImage=nhanvien.IdentifyImage.ToArray();
                //pictureEdit1.Image = byteToImage(arrImage);
            }
            catch(NullReferenceException)
            { }
            catch(InvalidCastException)
            { }
        }
        // thêm dữ liệu nhân viên
        #endregion
        #region Nhân viên
        /// <summary>
        /// thao tác trên nhân viên
        /// </summary>
        void addDataEm()
        {
            them = true;
            frmThemCapNhat_NhanVien frm = new frmThemCapNhat_NhanVien();
            frm.Text = "Thêm Nhân Viên";
            frm.ShowDialog();
            frmQLTT_NhanVien_Load(null, null);
            focusInARow(frm.employeeID);
        }
        // cập nhật dữ liệu
        void updateDataEm()
        {
            them = false;
            if (gridView1.SelectedRowsCount == 0)
            {
                MessageBox.Show("Bạn phải chọn một nhân viên để cập nhật", "Lỗi",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                frmThemCapNhat_NhanVien frm = new frmThemCapNhat_NhanVien();
                employeeID = lblEmployeeID.Text;
                frm.Text = "Cập Nhật Nhân Viên";
                frm.ShowDialog();
                frmQLTT_NhanVien_Load(null, null);
                focusInARow(frm.employeeID);
            }
        }
        void deleteDataEm()
        {
            if (gridView1.SelectedRowsCount == 0)
            {
                MessageBox.Show("Bạn phải chọn một nhân viên để xóa", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dtoEm = new dtoEmployees();
                bllEm = new bllEmployees();
                int r = gridView1.FocusedRowHandle;
                dtoEm.EmployeeID = gridView1.GetRowCellValue(r, gridView1.Columns["EmployeeID"]).ToString();
                DialogResult dialog = MessageBox.Show("Bạn thật sự muốn xóa nhân viên này không?", "Cảnh báo",
                   MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (dialog == DialogResult.OK)
                {
                    string err = "";
                    bool f = bllEm.deleteData(dtoEm, ref err);
                    if (!f)
                    {
                        MessageBox.Show("Xóa không thành công. Lỗi : " + err, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    frmQLTT_NhanVien_Load(null, null);
                }
            }
        }
        // xóa dữ liệu
        #endregion

        #region Loại nhân viên
        // thêm dữ liệu loại nhân viên
        void addDataEmType()
        {
            them = true;
            frmThemCapNhat_LoaiNV frmType = new frmThemCapNhat_LoaiNV();
            frmType.Text = "Thêm Loại Nhân Viên";
            frmType.ShowDialog();
            frmQLTT_NhanVien_Load(null, null);
            focusInARow(frmType.employeeTypeID);
        }

        
        // cập nhật dữ liệu
        void updateDataEmType()
        {
            them = false;
            if(gridView2.SelectedRowsCount==0)
            {
                MessageBox.Show("Bạn phải chọn một hàng loại nhân viên để cập nhật", "Lỗi",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                frmThemCapNhat_LoaiNV frmType = new frmThemCapNhat_LoaiNV();
                frmType.Text = "Cập Nhật Loại Nhân Viên";
                employeeTypeID = gridView2.GetFocusedRowCellValue(gridView2.Columns["EmployeeTypeID"]).ToString();
                frmType.ShowDialog();
                frmQLTT_NhanVien_Load(null, null);
                focusInARow(frmType.employeeTypeID);
            }
        }

        // xóa dữ liệu
       
        void deleteDataEmType()
        {
            if (gridView2.SelectedRowsCount == 0)
            {
                MessageBox.Show("Bạn phải chọn một loại nhân viên để xóa", "Lỗi",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dtoEmType = new dtoEmployeeTypes();
                bllEmType = new bllEmployeeTypes();
                int r = gridView2.FocusedRowHandle;
                dtoEmType.EmployeeTypeID = gridView2.GetRowCellValue(r, gridView2.Columns["EmployeeTypeID"]).ToString();
                DialogResult dialog = MessageBox.Show("Bạn thật sự muốn xóa loại nhân viên này không?", "Cảnh báo",
                   MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (dialog == DialogResult.OK)
                {
                    string err = "";
                    bool f = bllEmType.deleteData(dtoEmType, ref err);
                    if (!f)
                    {
                        MessageBox.Show("Xóa không thành công. Lỗi : " + err, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    frmQLTT_NhanVien_Load(null, null);
                }
            }
        }
        #endregion
        
      
        #region các sự kiện
        public frmQLTT_NhanVien()
        {
            InitializeComponent();
        }

        private void frmQLTT_NhanVien_Load(object sender, EventArgs e)
        {
            loadData();
            gridView1_Click(null, null);
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            bindingData();
        }

        private void btnAddEm_Click(object sender, EventArgs e)
        {
            addDataEm();
        }

        private void btnUpdateEm_Click(object sender, EventArgs e)
        {
            updateDataEm();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            frmQLTT_NhanVien_Load(null, null);
        }

        private void btnAddEmType_Click(object sender, EventArgs e)
        {
            addDataEmType();
        }

        private void btnUpdateEmType_Click(object sender, EventArgs e)
        {
            updateDataEmType();
        }

        private void btnDelEmType_Click(object sender, EventArgs e)
        {
            deleteDataEmType();
        }

        private void btnDelEm_Click(object sender, EventArgs e)
        {
            deleteDataEm();
        }

        private void gridView2_Click(object sender, EventArgs e)
        {
            try
            {
                dbs = new ShopCommerceDataContext();
                // dòng đang được trỏ vào
                int r = gridView2.FocusedRowHandle;
                // click vào loại khách hàng nào thì gridview kh sẽ hiển thị loại kh đó
                string EmployeeTypeID = gridView2.GetRowCellValue(r, gridView2.Columns["EmployeeTypeID"]).ToString();
                var cus = from n in dbs.Employees
                          where n.EmployeeTypeID == EmployeeTypeID
                          select n;
                gridControl2.DataSource = cus;
            }
            catch (NullReferenceException)
            { }
        }



    }
}
        #endregion