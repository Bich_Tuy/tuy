﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticBLL;
using ExoticDAL;
using ExoticDTO;

namespace ProjectExotic
{
    
    public partial class frmThemCapNhat_LoaiNV : DevExpress.XtraEditors.XtraForm
    {
        #region các thuộc tính và hàm
        public string employeeTypeID;// thuộc tính dùng để truyền vào form chính
        ShopCommerceDataContext dbs = null;
        dtoEmployeeTypes dtoEmType = null;
        bllEmployeeTypes bllEmType = null;
        bool them = frmQLTT_NhanVien.them;

        // đặt trạng thái form khi thêm/cập nhật dữ liệu
        void setPanel()
        {
            if(them==true)
            {
                txtEmployeeTypeID.Enabled = true;
            }
            else
            {
                txtEmployeeTypeID.Enabled = false;
                dbs = new ShopCommerceDataContext();
                // truyền dữ liệu
                employeeTypeID = frmQLTT_NhanVien.employeeTypeID;
                // tạo đối tượng
                EmployeeType data = (from n in dbs.EmployeeTypes
                                     where n.EmployeeTypeID == employeeTypeID
                                     select n).Single();
                // binding dữ liệu
                txtEmployeeTypeID.Text = data.EmployeeTypeID;
                txtEmployTypeName.Text = data.EmployeeTypeName;
                txtSalaryCoefficient.Text = Convert.ToString(data.SalaryCoefficient);
            }
        }
        // thêm dữ liệu
        void addDataEmType()
        {
            dtoEmType = new dtoEmployeeTypes();
            bllEmType = new bllEmployeeTypes();
            string err = "";
            // nạp dữ liệu
            dtoEmType.EmployeeTypeID = txtEmployeeTypeID.Text.Trim(); ;
            dtoEmType.EmployeeTypeName = txtEmployTypeName.Text.Trim();
            dtoEmType.SalaryCoefficient =float.Parse(txtSalaryCoefficient.Text);
            bool f = bllEmType.insertData(dtoEmType, ref err);
            if(f)
            {
                MessageBox.Show("Thêm dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                employeeTypeID=dtoEmType.EmployeeTypeID;
                this.Close();
            }
            else
            {
                MessageBox.Show("Thêm dữ liệu thất bại", "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmployTypeName.Focus();
            }
        }
        // cập nhật dữ liệu
        void updateDataEmType()
        {
            dtoEmType = new dtoEmployeeTypes();
            bllEmType = new bllEmployeeTypes();
            string err = "";
            // nạp dữ liệu
            dtoEmType.EmployeeTypeID = txtEmployeeTypeID.Text.Trim(); ;
            dtoEmType.EmployeeTypeName = txtEmployTypeName.Text.Trim();
            dtoEmType.SalaryCoefficient = float.Parse(txtSalaryCoefficient.Text);
            bool f = bllEmType.updateData(dtoEmType, ref err);
            if (f)
            {
                MessageBox.Show("Thêm dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                employeeTypeID = dtoEmType.EmployeeTypeID;
                this.Close();
            }
            else
            {
                MessageBox.Show("Thêm dữ liệu thất bại", "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmployTypeName.Focus();
            }
        }

        // kiểm tra dữ liệu
        void checkDataEmType()
        {
            bool right = true;// biến dùng để kiểm tra dữ liệu
            if(txtEmployeeTypeID.Text.Length>10)
            {
                right = false;
                MessageBox.Show("Độ dài của mã loại nhân viên không được quá 10 kí tự. ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtEmployTypeName.Text.Length>50)
            {
                right = false;
                MessageBox.Show("Độ dài tên loại nhân viên không được quá 50 kí tự. ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(right)
            {
                if(them)
                {
                    addDataEmType();
                }
                else
                {
                    updateDataEmType();
                }
            }

        }
        #endregion
        #region các sự kiện
        public frmThemCapNhat_LoaiNV()
        {
            InitializeComponent();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            checkDataEmType();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Có thể bạn chưa lưu dữ liệu.\nBạn có thật sự muốn thoát hay không?", "Thoát",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialog == DialogResult.OK)
            {
                this.Dispose();
                this.Close();
            }
            else
            {
                txtEmployeeTypeID.Focus();
            }

        }

        private void frmThemCapNhat_LoaiNV_Load(object sender, EventArgs e)
        {
            setPanel();
        }

        private void txtEmployeeTypeID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEmployTypeName.Focus();
        }

        private void txtEmployTypeName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtSalaryCoefficient.Focus();
        }

    }
        #endregion
}