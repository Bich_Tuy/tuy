﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticBLL;
using ExoticDAL;
using ExoticDTO;

namespace ProjectExotic
{
    public partial class frmThemCapNhat_LoaiKH : DevExpress.XtraEditors.XtraForm
    {
        public string customerTypeID;// thuộc tính ddeert truyền vào form chính
        ShopCommerceDataContext dbs = null;
        dtoCustomerTypes dtoCusType = null;
        bllCustomerTypes bllCusType = null;
        bool them = frmQLTT_KhachHang.them;

        // đặt trạng thái form khi thêm/ cập nhật dữ liệu
        void setPanel()
        {
            if(them==true)
            {
                txtCustomerTypeID.Enabled = true;
            }
            else
            {
                txtCustomerTypeID.Enabled = false;
                dbs = new ShopCommerceDataContext();
                // truyền dữ liệu
                customerTypeID = frmQLTT_KhachHang.customerTypeID;
                // load dữ liệu lên form 
               CustomerType data = (from n in dbs.CustomerTypes
                                 where n.CustomerTypeID == customerTypeID
                                 select n).Single();
               // binding dữ liệu
               txtCustomerTypeID.Text = data.CustomerTypeID;
               txtCustomerTypeName.Text = data.CustomerTypeName;
               txtDiscountRatio.Text =Convert.ToString( data.DiscountRatio);
                            
            }
        }
        // check data
        void checkData()
        {
            bool right = true;// biến dùng để kiểm tra dữ liệu
            if (txtCustomerTypeID.Text.Length > 10)
            {
                right = false;
                MessageBox.Show("Độ dài của mã loại khách hàng không được quá 10 kí tự.", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (txtCustomerTypeName.Text.Length > 20)
            {
                right = false;
                MessageBox.Show("Độ dài của Loại Khách hàng không được quá 20 kí tự.  ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (right)
            {
                if (them)
                {
                    addData();
                }
                else
                {
                    updateData();
                }
            }

        }
        void addData()
        {
            dtoCusType = new dtoCustomerTypes();
            bllCusType = new bllCustomerTypes();
            string err = "";
            // nạp dữ liệu
            dtoCusType.CustomerTypeID = txtCustomerTypeID.Text.Trim();
            dtoCusType.CustomerTypeName = txtCustomerTypeName.Text.Trim();
            dtoCusType.DiscountRatio = float.Parse(txtDiscountRatio.Text);
            bool f = bllCusType.insertData(dtoCusType, ref err);
            if (f)
            {
                MessageBox.Show("Thêm dữ liệu thành công", "Thành công",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                customerTypeID = dtoCusType.CustomerTypeID;
                this.Close();
            }
            else
            {
                MessageBox.Show("Thêm dữ liệu thất bại", "Thất bại",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCustomerTypeName.Focus();
            }
        }
        void updateData()
        {
            dtoCusType = new dtoCustomerTypes();
            bllCusType = new bllCustomerTypes();
            string err = "";
            // nạp dữ liệu
            dtoCusType.CustomerTypeID = txtCustomerTypeID.Text.Trim();
            dtoCusType.CustomerTypeName = txtCustomerTypeName.Text.Trim();
            dtoCusType.DiscountRatio = float.Parse(txtDiscountRatio.Text.Trim());
            bool f = bllCusType.updateData(dtoCusType, ref err);
            if (f)
            {
                MessageBox.Show("Cập nhật dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Cập nhật dữ liệu thất bại", "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCustomerTypeName.Focus();
            }
        }
        public frmThemCapNhat_LoaiKH()
        {
            InitializeComponent();
        }
        

        private void txtCustomerTypeID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCustomerTypeName.Focus();
        }

        private void txtCustomerTypeName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDiscountRatio.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            checkData();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Có thể bạn chưa lưu dữ liệu.\nBạn có thật sự muốn thoát hay không?", "Thoát",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialog == DialogResult.OK)
            {
                this.Dispose();
                this.Close();
            }
            else
            {
                txtCustomerTypeID.Focus();
            }
        }

        private void frmThemCapNhat_LoaiKH_Load(object sender, EventArgs e)
        {
            dbs = new ShopCommerceDataContext();
            setPanel();

        }
    }
}