﻿namespace ProjectExotic
{
    partial class frmQLTT_NhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQLTT_NhanVien));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnReload = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnPrintEm = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnUpdateEm = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddEm = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelEm = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnUpdateEmType = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelEmType = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddEmType = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.EmployeeTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EmployeeTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SalaryCoefficient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblName = new System.Windows.Forms.Label();
            this.lblEmployeeType = new System.Windows.Forms.Label();
            this.lblEmployeeID = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblIDNumber = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblHireday = new System.Windows.Forms.Label();
            this.lblBirthday = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.EmployID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EmployTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Birthday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Hireday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Gender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IDNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Address = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Phone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnReload);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.btnPrintEm);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btnUpdateEm);
            this.panel1.Controls.Add(this.btnAddEm);
            this.panel1.Controls.Add(this.btnDelEm);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.btnUpdateEmType);
            this.panel1.Controls.Add(this.btnDelEmType);
            this.panel1.Controls.Add(this.btnAddEmType);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 424);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(944, 61);
            this.panel1.TabIndex = 0;
            // 
            // btnReload
            // 
            this.btnReload.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReload.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnReload.Appearance.Options.UseBackColor = true;
            this.btnReload.Appearance.Options.UseFont = true;
            this.btnReload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnReload.Image = ((System.Drawing.Image)(resources.GetObject("btnReload.Image")));
            this.btnReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnReload.Location = new System.Drawing.Point(863, 2);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(68, 57);
            this.btnReload.TabIndex = 29;
            this.btnReload.Text = "Tải lại";
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(856, 11);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1, 50);
            this.pictureBox2.TabIndex = 28;
            this.pictureBox2.TabStop = false;
            // 
            // btnPrintEm
            // 
            this.btnPrintEm.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPrintEm.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnPrintEm.Appearance.Options.UseBackColor = true;
            this.btnPrintEm.Appearance.Options.UseFont = true;
            this.btnPrintEm.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnPrintEm.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintEm.Image")));
            this.btnPrintEm.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnPrintEm.Location = new System.Drawing.Point(708, 4);
            this.btnPrintEm.Name = "btnPrintEm";
            this.btnPrintEm.Size = new System.Drawing.Size(142, 57);
            this.btnPrintEm.TabIndex = 27;
            this.btnPrintEm.Text = "In Danh Sách Nhân Viên";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(697, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1, 50);
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // btnUpdateEm
            // 
            this.btnUpdateEm.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnUpdateEm.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnUpdateEm.Appearance.Options.UseBackColor = true;
            this.btnUpdateEm.Appearance.Options.UseFont = true;
            this.btnUpdateEm.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnUpdateEm.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateEm.Image")));
            this.btnUpdateEm.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnUpdateEm.Location = new System.Drawing.Point(473, 3);
            this.btnUpdateEm.Name = "btnUpdateEm";
            this.btnUpdateEm.Size = new System.Drawing.Size(88, 57);
            this.btnUpdateEm.TabIndex = 21;
            this.btnUpdateEm.Text = "Sửa Nhân Viên";
            this.btnUpdateEm.Click += new System.EventHandler(this.btnUpdateEm_Click);
            // 
            // btnAddEm
            // 
            this.btnAddEm.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAddEm.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddEm.Appearance.Options.UseBackColor = true;
            this.btnAddEm.Appearance.Options.UseFont = true;
            this.btnAddEm.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddEm.Image = ((System.Drawing.Image)(resources.GetObject("btnAddEm.Image")));
            this.btnAddEm.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnAddEm.Location = new System.Drawing.Point(368, 0);
            this.btnAddEm.Name = "btnAddEm";
            this.btnAddEm.Size = new System.Drawing.Size(99, 57);
            this.btnAddEm.TabIndex = 19;
            this.btnAddEm.Text = "Thêm Nhân Viên";
            this.btnAddEm.Click += new System.EventHandler(this.btnAddEm_Click);
            // 
            // btnDelEm
            // 
            this.btnDelEm.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDelEm.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDelEm.Appearance.Options.UseBackColor = true;
            this.btnDelEm.Appearance.Options.UseFont = true;
            this.btnDelEm.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnDelEm.Image = ((System.Drawing.Image)(resources.GetObject("btnDelEm.Image")));
            this.btnDelEm.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDelEm.Location = new System.Drawing.Point(580, 0);
            this.btnDelEm.Name = "btnDelEm";
            this.btnDelEm.Size = new System.Drawing.Size(91, 57);
            this.btnDelEm.TabIndex = 20;
            this.btnDelEm.Text = "Xóa Nhân Viên";
            this.btnDelEm.Click += new System.EventHandler(this.btnDelEm_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(334, 7);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1, 50);
            this.pictureBox3.TabIndex = 25;
            this.pictureBox3.TabStop = false;
            // 
            // btnUpdateEmType
            // 
            this.btnUpdateEmType.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnUpdateEmType.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnUpdateEmType.Appearance.Options.UseBackColor = true;
            this.btnUpdateEmType.Appearance.Options.UseFont = true;
            this.btnUpdateEmType.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnUpdateEmType.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateEmType.Image")));
            this.btnUpdateEmType.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnUpdateEmType.Location = new System.Drawing.Point(145, 2);
            this.btnUpdateEmType.Name = "btnUpdateEmType";
            this.btnUpdateEmType.Size = new System.Drawing.Size(88, 57);
            this.btnUpdateEmType.TabIndex = 24;
            this.btnUpdateEmType.Text = "Sửa loại NV";
            this.btnUpdateEmType.Click += new System.EventHandler(this.btnUpdateEmType_Click);
            // 
            // btnDelEmType
            // 
            this.btnDelEmType.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDelEmType.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDelEmType.Appearance.Options.UseBackColor = true;
            this.btnDelEmType.Appearance.Options.UseFont = true;
            this.btnDelEmType.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnDelEmType.Image = ((System.Drawing.Image)(resources.GetObject("btnDelEmType.Image")));
            this.btnDelEmType.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDelEmType.Location = new System.Drawing.Point(236, 2);
            this.btnDelEmType.Name = "btnDelEmType";
            this.btnDelEmType.Size = new System.Drawing.Size(90, 57);
            this.btnDelEmType.TabIndex = 23;
            this.btnDelEmType.Text = "Xóa loại NV";
            this.btnDelEmType.Click += new System.EventHandler(this.btnDelEmType_Click);
            // 
            // btnAddEmType
            // 
            this.btnAddEmType.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAddEmType.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddEmType.Appearance.Options.UseBackColor = true;
            this.btnAddEmType.Appearance.Options.UseFont = true;
            this.btnAddEmType.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAddEmType.Image = ((System.Drawing.Image)(resources.GetObject("btnAddEmType.Image")));
            this.btnAddEmType.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnAddEmType.Location = new System.Drawing.Point(43, 3);
            this.btnAddEmType.Name = "btnAddEmType";
            this.btnAddEmType.Size = new System.Drawing.Size(96, 57);
            this.btnAddEmType.TabIndex = 22;
            this.btnAddEmType.Text = "Thêm loại NV";
            this.btnAddEmType.Click += new System.EventHandler(this.btnAddEmType_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gridControl1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(236, 424);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin loại NV";
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(3, 17);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(230, 404);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.EmployeeTypeID,
            this.EmployeeTypeName,
            this.SalaryCoefficient});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.Click += new System.EventHandler(this.gridView2_Click);
            // 
            // EmployeeTypeID
            // 
            this.EmployeeTypeID.Caption = "Mã Loại NV";
            this.EmployeeTypeID.FieldName = "EmployeeTypeID";
            this.EmployeeTypeID.Name = "EmployeeTypeID";
            this.EmployeeTypeID.Visible = true;
            this.EmployeeTypeID.VisibleIndex = 0;
            // 
            // EmployeeTypeName
            // 
            this.EmployeeTypeName.Caption = "Loại NV";
            this.EmployeeTypeName.FieldName = "EmployeeTypeName";
            this.EmployeeTypeName.Name = "EmployeeTypeName";
            this.EmployeeTypeName.Visible = true;
            this.EmployeeTypeName.VisibleIndex = 1;
            // 
            // SalaryCoefficient
            // 
            this.SalaryCoefficient.Caption = "Hệ số lương";
            this.SalaryCoefficient.FieldName = "SalaryCoefficient";
            this.SalaryCoefficient.Name = "SalaryCoefficient";
            this.SalaryCoefficient.Visible = true;
            this.SalaryCoefficient.VisibleIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(708, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(236, 424);
            this.panel2.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tabControl1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(236, 424);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin nhân viên";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 17);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(230, 404);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblName);
            this.tabPage1.Controls.Add(this.lblEmployeeType);
            this.tabPage1.Controls.Add(this.lblEmployeeID);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.pictureEdit1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(222, 378);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin chung";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.Blue;
            this.lblName.Location = new System.Drawing.Point(53, 337);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(42, 19);
            this.lblName.TabIndex = 16;
            this.lblName.Text = "label";
            // 
            // lblEmployeeType
            // 
            this.lblEmployeeType.AutoSize = true;
            this.lblEmployeeType.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeType.ForeColor = System.Drawing.Color.Blue;
            this.lblEmployeeType.Location = new System.Drawing.Point(53, 258);
            this.lblEmployeeType.Name = "lblEmployeeType";
            this.lblEmployeeType.Size = new System.Drawing.Size(42, 19);
            this.lblEmployeeType.TabIndex = 15;
            this.lblEmployeeType.Text = "label";
            // 
            // lblEmployeeID
            // 
            this.lblEmployeeID.AutoSize = true;
            this.lblEmployeeID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeID.ForeColor = System.Drawing.Color.Blue;
            this.lblEmployeeID.Location = new System.Drawing.Point(53, 189);
            this.lblEmployeeID.Name = "lblEmployeeID";
            this.lblEmployeeID.Size = new System.Drawing.Size(42, 19);
            this.lblEmployeeID.TabIndex = 14;
            this.lblEmployeeID.Text = "label";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 303);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 19);
            this.label3.TabIndex = 13;
            this.label3.Text = "3. Tên:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 226);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 19);
            this.label2.TabIndex = 12;
            this.label2.Text = "2. Loại NV:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 19);
            this.label1.TabIndex = 11;
            this.label1.Text = "1. Mã NV:";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(51, 33);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(100, 96);
            this.pictureEdit1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblIDNumber);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.lblGender);
            this.tabPage2.Controls.Add(this.lblHireday);
            this.tabPage2.Controls.Add(this.lblBirthday);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(222, 379);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Chi Tiết";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblIDNumber
            // 
            this.lblIDNumber.AutoSize = true;
            this.lblIDNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIDNumber.ForeColor = System.Drawing.Color.Blue;
            this.lblIDNumber.Location = new System.Drawing.Point(46, 322);
            this.lblIDNumber.Name = "lblIDNumber";
            this.lblIDNumber.Size = new System.Drawing.Size(42, 19);
            this.lblIDNumber.TabIndex = 21;
            this.lblIDNumber.Text = "label";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(16, 270);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 19);
            this.label9.TabIndex = 20;
            this.label9.Text = "4. CMND:";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.ForeColor = System.Drawing.Color.Blue;
            this.lblGender.Location = new System.Drawing.Point(46, 222);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(42, 19);
            this.lblGender.TabIndex = 19;
            this.lblGender.Text = "label";
            // 
            // lblHireday
            // 
            this.lblHireday.AutoSize = true;
            this.lblHireday.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHireday.ForeColor = System.Drawing.Color.Blue;
            this.lblHireday.Location = new System.Drawing.Point(42, 135);
            this.lblHireday.Name = "lblHireday";
            this.lblHireday.Size = new System.Drawing.Size(42, 19);
            this.lblHireday.TabIndex = 18;
            this.lblHireday.Text = "label";
            // 
            // lblBirthday
            // 
            this.lblBirthday.AutoSize = true;
            this.lblBirthday.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBirthday.ForeColor = System.Drawing.Color.Blue;
            this.lblBirthday.Location = new System.Drawing.Point(35, 49);
            this.lblBirthday.Name = "lblBirthday";
            this.lblBirthday.Size = new System.Drawing.Size(42, 19);
            this.lblBirthday.TabIndex = 17;
            this.lblBirthday.Text = "label";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 19);
            this.label6.TabIndex = 14;
            this.label6.Text = "3. Giới tính:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 19);
            this.label5.TabIndex = 15;
            this.label5.Text = "1. Ngày sinh:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 19);
            this.label4.TabIndex = 16;
            this.label4.Text = "2. Ngày vào làm:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lblPhone);
            this.tabPage3.Controls.Add(this.lblAddress);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(222, 379);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Liên Hệ";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.ForeColor = System.Drawing.Color.Blue;
            this.lblPhone.Location = new System.Drawing.Point(57, 211);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(42, 19);
            this.lblPhone.TabIndex = 19;
            this.lblPhone.Text = "label";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.ForeColor = System.Drawing.Color.Blue;
            this.lblAddress.Location = new System.Drawing.Point(57, 107);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(42, 19);
            this.lblAddress.TabIndex = 18;
            this.lblAddress.Text = "label";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(28, 164);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 19);
            this.label8.TabIndex = 17;
            this.label8.Text = "2. SĐT:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 19);
            this.label7.TabIndex = 16;
            this.label7.Text = "1. Địa chỉ:";
            // 
            // gridControl2
            // 
            this.gridControl2.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(236, 0);
            this.gridControl2.MainView = this.gridView1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(472, 424);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.EmployID,
            this.EmployTypeName,
            this.FirstName,
            this.LastName,
            this.Birthday,
            this.Hireday,
            this.Gender,
            this.IDNumber,
            this.Address,
            this.Phone});
            this.gridView1.GridControl = this.gridControl2;
            this.gridView1.Name = "gridView1";
            // 
            // EmployID
            // 
            this.EmployID.Caption = "Mã Nhân Viên";
            this.EmployID.FieldName = "EmployeeID";
            this.EmployID.Name = "EmployID";
            this.EmployID.Visible = true;
            this.EmployID.VisibleIndex = 0;
            // 
            // EmployTypeName
            // 
            this.EmployTypeName.Caption = "Loại Nhân Viên";
            this.EmployTypeName.FieldName = "EmployeeTypeName";
            this.EmployTypeName.Name = "EmployTypeName";
            this.EmployTypeName.Visible = true;
            this.EmployTypeName.VisibleIndex = 1;
            // 
            // FirstName
            // 
            this.FirstName.Caption = "Họ";
            this.FirstName.FieldName = "FirstName";
            this.FirstName.Name = "FirstName";
            this.FirstName.Visible = true;
            this.FirstName.VisibleIndex = 2;
            // 
            // LastName
            // 
            this.LastName.Caption = "Tên";
            this.LastName.FieldName = "LastName";
            this.LastName.Name = "LastName";
            this.LastName.Visible = true;
            this.LastName.VisibleIndex = 3;
            // 
            // Birthday
            // 
            this.Birthday.Caption = "Ngày Sinh";
            this.Birthday.FieldName = "Birthday";
            this.Birthday.Name = "Birthday";
            this.Birthday.Visible = true;
            this.Birthday.VisibleIndex = 4;
            // 
            // Hireday
            // 
            this.Hireday.Caption = "Ngày vào làm";
            this.Hireday.FieldName = "Hireday";
            this.Hireday.Name = "Hireday";
            this.Hireday.Visible = true;
            this.Hireday.VisibleIndex = 5;
            // 
            // Gender
            // 
            this.Gender.Caption = "Giới Tính";
            this.Gender.FieldName = "Gender";
            this.Gender.Name = "Gender";
            this.Gender.Visible = true;
            this.Gender.VisibleIndex = 6;
            // 
            // IDNumber
            // 
            this.IDNumber.Caption = "CMND";
            this.IDNumber.FieldName = "IDNumber";
            this.IDNumber.Name = "IDNumber";
            this.IDNumber.Visible = true;
            this.IDNumber.VisibleIndex = 7;
            // 
            // Address
            // 
            this.Address.Caption = "Địa Chỉ";
            this.Address.FieldName = "Address";
            this.Address.Name = "Address";
            this.Address.Visible = true;
            this.Address.VisibleIndex = 8;
            // 
            // Phone
            // 
            this.Phone.Caption = "Phone";
            this.Phone.FieldName = "Phone";
            this.Phone.Name = "Phone";
            this.Phone.Visible = true;
            this.Phone.VisibleIndex = 9;
            // 
            // frmQLTT_NhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 485);
            this.Controls.Add(this.gridControl2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmQLTT_NhanVien";
            this.Load += new System.EventHandler(this.frmQLTT_NhanVien_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btnUpdateEm;
        private DevExpress.XtraEditors.SimpleButton btnAddEm;
        private DevExpress.XtraEditors.SimpleButton btnDelEm;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraEditors.SimpleButton btnUpdateEmType;
        private DevExpress.XtraEditors.SimpleButton btnDelEmType;
        private DevExpress.XtraEditors.SimpleButton btnAddEmType;
        private DevExpress.XtraEditors.SimpleButton btnReload;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.SimpleButton btnPrintEm;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn EmployeeTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn EmployeeTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn SalaryCoefficient;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblEmployeeType;
        private System.Windows.Forms.Label lblEmployeeID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblHireday;
        private System.Windows.Forms.Label lblBirthday;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblIDNumber;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn EmployID;
        private DevExpress.XtraGrid.Columns.GridColumn EmployTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn FirstName;
        private DevExpress.XtraGrid.Columns.GridColumn LastName;
        private DevExpress.XtraGrid.Columns.GridColumn Birthday;
        private DevExpress.XtraGrid.Columns.GridColumn Hireday;
        private DevExpress.XtraGrid.Columns.GridColumn Gender;
        private DevExpress.XtraGrid.Columns.GridColumn IDNumber;
        private DevExpress.XtraGrid.Columns.GridColumn Address;
        private DevExpress.XtraGrid.Columns.GridColumn Phone;
    }
}