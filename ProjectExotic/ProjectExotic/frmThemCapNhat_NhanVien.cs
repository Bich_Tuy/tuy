﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using ExoticBLL;
using ExoticDAL;
using ExoticDTO;


namespace ProjectExotic
{
    public partial class frmThemCapNhat_NhanVien : DevExpress.XtraEditors.XtraForm
    {
        #region các phương thức và thuộc tính
        public string employeeID;
        ShopCommerceDataContext dbs = null;
        bllEmployees bllEm = null;
        dtoEmployees dtoEm = null;
        MemoryStream ms = new MemoryStream();
        byte[] arrImage;
        bool them=frmQLTT_NhanVien.them;
        //// phương thức chuyển đổi kiểu byte[] sang kiểu Image
        //public Image byteArrayToImage(byte[] byteArrayIn)
        //{

        //    MemoryStream ms = new MemoryStream(byteArrayIn);
        //    Image returnImage = Image.FromStream(ms);
        //    return returnImage;
        //}
        //image = Image.FromStream(new MemoryStream(buffer));
        //public byte[] imageToByArray(System.Drawing.Image ImageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    ImageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
        //    return ms.ToArray();
        //}


        // đặt trạng thái form khi thêm/cập nhật dữ liệu
        void setPanel()
        {
            if(them==true)
            {
                txtEmployID.Enabled = false;
                pictureEdit1.Image = System.Drawing.Image.FromFile("..\\..\\Resources\\Untitled.png");
                ms = new MemoryStream();
                pictureEdit1.Image.Save(ms,pictureEdit1.Image.RawFormat);
                arrImage = ms.GetBuffer();
                ms.Close();
            }
            else
            {
                txtEmployID.Enabled = false;
                dbs = new ShopCommerceDataContext();
                
                //truyền dữ liệu
                employeeID = frmQLTT_NhanVien.employeeID;
                //tạo đối tượng
                Employee data = (from n in dbs.Employees
                                 where n.EmployeeID == frmQLTT_NhanVien.employeeID
                                 select n).SingleOrDefault();
                // binding dữ liệu
                try
                
                {
                    txtEmployID.Text = data.EmployeeID;
                    cbEmployType.Text = data.EmployeeType.EmployeeTypeName;
                    txtFirstName.Text = data.FirstName;
                    txtLastName.Text = data.LastName;
                    dateBirthday.EditValue = data.Birthday;
                    dateHireday.EditValue = data.Hireday;
                    radioButtonNam.Checked = Convert.ToBoolean(data.Gender);
                    radioButtonNu.Checked = Convert.ToBoolean(data.Gender);
                    txtIDNumber.Text = data.IDNumber;
                    txtAddress.Text = data.Address;
                    txtPhone.Text = data.Phone;
                    
                    //pictureEdit1.Image = byteArrayToImage(arrImage);
                }
                catch (NullReferenceException)
                { }
                catch (InvalidOperationException)
                { }
                
            }
        }

        //kiểm tra dữ liệu
        void checkData()
        {
            bool right = true;// biến dùng để kiểm tra dữ liệu
            if(txtEmployID.Text.Length>10)
            {
                 right = false;
                MessageBox.Show("Độ dài của mã nhân viên không được quá 10 kí tự.  ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtFirstName.Text.Length>30)
            {
                 right = false;
                MessageBox.Show("Độ dài của họ và tên lót không được quá 30 kí tự ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtLastName.Text.Length>10)
            {
                 right = false;
                MessageBox.Show("Độ dài của tên không được quá 10 kí tự", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtAddress.Text.Length>50)
            {
                 right = false;
                MessageBox.Show("Độ dài của địa chỉ không quá 50 kí tự", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(txtPhone.Text.Length>15)
            {
                 right = false;
                MessageBox.Show("Độ dài của số điện thoại không quá 15 kí tự. \nChúng tôi khuyên bạn điền BrandID gồm 3 chữ cái in hoa ", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // kiểm tra định dạng số điện thoại
            

            //kiểm tra ngày sinh
            if(dateBirthday.DateTime.Year<=1900)
            {
                right = false;
                MessageBox.Show("năm sinh không được nhỏ hơn 1900", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if((dateHireday.DateTime.Year-dateBirthday.DateTime.Year)<=18)
            {
                right = false;
                MessageBox.Show("nhân viên phải đủ 18 tuổi trở lên\n vui lòng nhập lại dữ liệu", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (right)
            {
                if (them)
                {
                    addDataEm();
                }
                else
                {
                    updateDataEm();
                }
            }
         
        }
        void addDataEm()
        {
            dtoEm = new dtoEmployees();
            bllEm = new bllEmployees();
            string err = "";
            // nạp dữ liệu
            dtoEm.EmployeeID = txtEmployID.Text.Trim();
            dtoEm.EmployTypeID = cbEmployType.SelectedValue.ToString();
            dtoEm.FirstName = txtFirstName.Text.Trim();
            dtoEm.LastName = txtLastName.Text.Trim();
            dtoEm.Birthday = Convert.ToDateTime(dateBirthday.EditValue);
            dtoEm.Hireday = Convert.ToDateTime(dateHireday.EditValue);
            dtoEm.IdentifyImage = arrImage == null ? null : arrImage;
            //string gender = "";
            if (radioButtonNam.Checked == true)
                //gender="nam";
                dtoEm.Gender = false;
            else
                dtoEm.Gender = true;
            
            dtoEm.Address = txtAddress.Text.Trim();
            dtoEm.Phone = txtPhone.Text.Trim();
            dtoEm.IDNumber = txtIDNumber.Text.Trim();
            dtoEm.IdentifyImage = arrImage == null ? null : arrImage;
            bool f = bllEm.insertData(dtoEm, ref err);
            if(f==true)
            {
                MessageBox.Show("Thêm dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                employeeID=dtoEm.EmployeeID;
                this.Close();
            }
            else
            {
                MessageBox.Show("Thêm dữ liệu thất bại"+err, "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        void updateDataEm()
        {
            dtoEm = new dtoEmployees();
            bllEm = new bllEmployees();
            string err = "";
            // nạp dữ liệu
            dtoEm.EmployeeID = txtEmployID.Text.Trim();
            dtoEm.EmployTypeID = cbEmployType.SelectedValue.ToString();
            dtoEm.FirstName = txtFirstName.Text.Trim();
            dtoEm.LastName = txtLastName.Text.Trim();
            dtoEm.Birthday = Convert.ToDateTime(dateBirthday.EditValue);
            dtoEm.Hireday = Convert.ToDateTime(dateHireday.EditValue);
            if (radioButtonNam.Checked == true)
            {
                dtoEm.Gender = false;
            }
            else
                dtoEm.Gender = true;
            dtoEm.Address = txtAddress.Text.Trim();
            dtoEm.Phone = txtPhone.Text.Trim();
            dtoEm.IDNumber = txtIDNumber.Text.Trim();
            bool f = bllEm.updateData(dtoEm, ref err);
            dtoEm.IdentifyImage = arrImage == null ? null : arrImage;
            if (f == true)
            {
                MessageBox.Show("cập nhật dữ liệu thành công"+err, "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                employeeID = dtoEm.EmployeeID;
                this.Close();
            }
            else
            {
                MessageBox.Show("cập nhật dữ liệu thất bại", "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        

        #endregion
        public frmThemCapNhat_NhanVien()
        {
            InitializeComponent();
        }

        private void frmThemCapNhat_NhanVien_Load(object sender, EventArgs e)
        {
            
            dbs = new ShopCommerceDataContext();
            // đổ dữ liệu vào combobox
            var data = from n in dbs.EmployeeTypes
                       select n;
            cbEmployType.DataSource = data;
            cbEmployType.DisplayMember = "EmployeeTypeName";
            cbEmployType.ValueMember = "EmployeeTypeID";
            if(this.Text=="Thêm Nhân Viên")
            {
                them = true;
            }
            else
            {
                them = false;
                setPanel();
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            checkData();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Có thể bạn chưa lưu dữ liệu.\nBạn có thật sự muốn thoát hay không?", "Thoát",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dialog == DialogResult.OK)
            {
                this.Dispose();
                this.Close();
            }
            else
            {
                txtEmployID.Focus();
            }
        }

        private void btnGetImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog odlgOpenFile = new OpenFileDialog();
            odlgOpenFile.InitialDirectory = "C:\\";
            odlgOpenFile.Title = "Image files (*.jpg)|*.jpg|All files (*.*)|*.*";
            if(odlgOpenFile.ShowDialog()==DialogResult.OK)
            {
                pictureEdit1.Image = System.Drawing.Image.FromFile(odlgOpenFile.FileName);
                ms = new MemoryStream();
                pictureEdit1.Image.Save(ms, pictureEdit1.Image.RawFormat);
                arrImage = ms.GetBuffer();
                ms.Close();
            }
        }
        #region sử dụng phím enter khi nhập liệu
        private void txtEmployID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cbEmployType.Focus();
        }

        private void cbEmployType_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFirstName.Focus();
        }

        private void txtFirstName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtLastName.Focus();
        }

        private void txtLastName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dateBirthday.Focus();
        }

        private void txtAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPhone.Focus();
        }
        #endregion
    }
}