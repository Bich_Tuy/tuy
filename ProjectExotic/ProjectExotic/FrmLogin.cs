﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticDAL;

namespace ProjectExotic
{
    public partial class FrmLogin : DevExpress.XtraEditors.XtraForm
    {
        //------------Các biến---------------
        ShopCommerceDataContext dbs = new ShopCommerceDataContext();//Biến của class LINQ to SQL
        public static bool success = false; //Xét xem đăng nhập có thành công hay không
        public static string tenDangNhap; //Tên đăng nhập thành công
        public static string userName; //Tài khoản đăng nhập thành công
        public static int quyenTruyCap;//Phân quyền truy cập
        //------------Các hàm----------------
        void Login()
        {
            //--------------Kiểm tra thông tin-----------------
            var data = from n in dbs.Employees
                       where n.Username==txtTaiKhoan.Text.Trim() && n.Password==txtMatKhau.Text.Trim()
                       select n;
            if(data.Any())
            {
                success = true;
                
            }
            else
            {
                //Hiện câu thông báo lỗi
                MessageBox.Show("Tên đăng nhập hay mật khẩu của bạn đã nhập vào không đúng.\n"
                    + "Xin mời nhập lại.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtTaiKhoan.Focus();
            }
            //-----------------Phân quyền truy cập-----------------------
            // 1 là Administrator. Mã là ADD
            // 2 là Employer. Mã là EER
            // 3 là Employee. Mã là EEE
            if(success==true)
            {
                string EmployeeTypeID = data.First().EmployeeTypeID;
                if (EmployeeTypeID == "ADD") //người đăng nhập là admin
                    quyenTruyCap = 1;
                else if (EmployeeTypeID == "EER")
                    quyenTruyCap = 2;
                else if(EmployeeTypeID == "EEE")
                    quyenTruyCap = 3;
                this.Close();
            }
        }
        //------------Các Sự kiện------------
        public FrmLogin()
        {
            InitializeComponent();
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            //Thoát Form
            //Nếu đăng nhập không thành công và click vào nút Thoát Thì Thoát Khỏi Form
            if (!success)
            {
                this.Dispose();
                Application.Exit();
            }
                
        }

        private void txtTaiKhoan_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Nếu nhấn phím Enter khi nhập xong txtTaiKhoan
            //Thì điều hướng tới txtMatKhau
            if (e.KeyChar == 13)
                txtMatKhau.Focus();
        }

        private void txtMatKhau_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Nếu nhấn phím Enterkhi nhập xong txtMatKhau
            //thì xảy ra sự kiện click btnDangNhap
            if (e.KeyChar == 13)
                btnDangNhap_Click(null, null);
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (success == false)
            {
                this.Dispose();
                Application.Exit();
            }
                
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            txtTaiKhoan.Text = "1";
            txtMatKhau.Text = "2";
        }
    }
}