﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticBLL;
using ExoticDAL;
using ExoticDTO;

namespace ProjectExotic
{
    public partial class frmDanhMuc_KhachHang : DevExpress.XtraEditors.XtraForm
    {
        ShopCommerceDataContext dbs = null;
        bllCustomers bll = null;// goi lop bll
        dtoCustomers dto = null;//
        bllCustomerTypes bllCusType = null;
        dtoCustomerTypes dtoCusType = null;

        // load du lieu
        void loadData()
        {
            bll = new bllCustomers();
            gridControlDM_KhachHang.DataSource = bll.getTable();
            bllCusType = new bllCustomerTypes();
            gridControl1.DataSource = bllCusType.getTable();
        }

        // binding du lieu tu gridview len panel thong tin khach hang
        void bindingData()
        {

            try
            {
                lblCustomerID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["CustomerID"]).ToString();
                lblCustomerType.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["CustomerTypeName"]).ToString();
                lblFirstName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["FirstName"]).ToString();
                lblLastName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["LastName"]).ToString();
                lblIDNumber.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["IDNumber"]).ToString();
                lblRegisterDate.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["RegisterDate"]).ToString();
                lblAddress.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Address"]).ToString();
                lblPhone.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone"]).ToString();
                // neu truong hop phone 1 null
                if(gridView1.Columns["Phone1"]==null)
                {
                    lblPhone1.Text = "";
                }
                else
                {
                    lblPhone1.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone1"]).ToString();
                }
                lblEmail.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Email"]).ToString();
                lblGender.Text = "0";
            }
            //tránh xảy ra lỗi khi 1 ô dữ liệu bằng null
            catch (NullReferenceException)
            {

            }
            catch (InvalidCastException)
            { }
        }
        // các su kien
        public frmDanhMuc_KhachHang()
        {
            InitializeComponent();
        }

        private void gridControlDM_KhachHang_Click(object sender, EventArgs e)
        {
            bindingData();
        }

        private void frmDanhMuc_KhachHang_Load(object sender, EventArgs e)
        {
            loadData();
            gridView1_Click(null, null);

        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            bindingData();
        }

        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            
        }

        private void gridView2_Click(object sender, EventArgs e)
        {
            try
            {
                dbs = new ShopCommerceDataContext();
                // dòng đang được trỏ vào
                int r = gridView2.FocusedRowHandle;
                // click vào loại khách hàng nào thì gridview kh sẽ hiển thị loại kh đó
                string CutomerTypeID = gridView2.GetRowCellValue(r, gridView2.Columns["CustomerTypeID"]).ToString();
                var cus = from n in dbs.Customers
                          where n.CustomerTypeID == CutomerTypeID
                          select n;
                gridControlDM_KhachHang.DataSource = cus;
            }
            catch(NullReferenceException)
            { }
        }


    }
}