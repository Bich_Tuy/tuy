﻿namespace ProjectExotic
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            this.tileBarGroup7 = new DevExpress.XtraBars.Navigation.TileBarGroup();
            this.tileBar1 = new DevExpress.XtraBars.Navigation.TileBar();
            this.tileBarGroup2 = new DevExpress.XtraBars.Navigation.TileBarGroup();
            this.btnHoatDong = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.tileBar_HoatDong = new DevExpress.XtraBars.Navigation.TileBarDropDownContainer();
            this.btnHoatDong_GiaoHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnHoatDong_TraHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnHoatDong_CNXuatHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnHoatDong_CNNhapHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnHoatDong_XuatHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnHoatDong_NhapHang = new DevExpress.XtraEditors.SimpleButton();
            this.tileBarItem2 = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.tileBar_DanhMuc = new DevExpress.XtraBars.Navigation.TileBarDropDownContainer();
            this.btnDanhMuc_ThongTinGiaoHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnDanhMuc_KhachHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnDanhMuc_HoaDonXuatHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnDanhMuc_HoaDonNhapHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnDanhMuc_Size = new DevExpress.XtraEditors.SimpleButton();
            this.btnDanhMuc_MatHang = new DevExpress.XtraEditors.SimpleButton();
            this.tileBarItem7 = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.tileBar_QuanLyNhapHang = new DevExpress.XtraBars.Navigation.TileBarDropDownContainer();
            this.btnQLNH_PhieuTraCongNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLNH_CongNoNhapHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLNH_HoaDonNhapHang = new DevExpress.XtraEditors.SimpleButton();
            this.tileBarItem8 = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.tilebar_QuanLyXuatHang = new DevExpress.XtraBars.Navigation.TileBarDropDownContainer();
            this.btnQLXH_ThongTinGiaoHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLXH_PhieuTraCongNo = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLXH_CongNoXuatHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLXH_HoaDonXuatHang = new DevExpress.XtraEditors.SimpleButton();
            this.tileBarItem3 = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.tileBar_QuanLyThongTin = new DevExpress.XtraBars.Navigation.TileBarDropDownContainer();
            this.btnQLTT_NhanVien = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLTT_KhachHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLTT_Size = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLTT_MatHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLTT_ThuongHieu = new DevExpress.XtraEditors.SimpleButton();
            this.btnQLTT_NhaCungCap = new DevExpress.XtraEditors.SimpleButton();
            this.tileBarItem4 = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.tileBarItem5 = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.tileBarItem6 = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.tileBarItem9 = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDangXuat = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            ((System.ComponentModel.ISupportInitialize)(this.tileBar_HoatDong)).BeginInit();
            this.tileBar_HoatDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tileBar_DanhMuc)).BeginInit();
            this.tileBar_DanhMuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tileBar_QuanLyNhapHang)).BeginInit();
            this.tileBar_QuanLyNhapHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tilebar_QuanLyXuatHang)).BeginInit();
            this.tilebar_QuanLyXuatHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tileBar_QuanLyThongTin)).BeginInit();
            this.tileBar_QuanLyThongTin.SuspendLayout();
            this.panelFooter.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // tileBarGroup7
            // 
            this.tileBarGroup7.Name = "tileBarGroup7";
            // 
            // tileBar1
            // 
            this.tileBar1.AllowDrag = false;
            this.tileBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tileBar1.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.tileBar1.Groups.Add(this.tileBarGroup2);
            this.tileBar1.Location = new System.Drawing.Point(0, 0);
            this.tileBar1.MaxId = 13;
            this.tileBar1.Name = "tileBar1";
            this.tileBar1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons;
            this.tileBar1.SelectionBorderWidth = 0;
            this.tileBar1.Size = new System.Drawing.Size(996, 101);
            this.tileBar1.TabIndex = 6;
            this.tileBar1.Text = "tileBar1";
            // 
            // tileBarGroup2
            // 
            this.tileBarGroup2.Items.Add(this.btnHoatDong);
            this.tileBarGroup2.Items.Add(this.tileBarItem2);
            this.tileBarGroup2.Items.Add(this.tileBarItem7);
            this.tileBarGroup2.Items.Add(this.tileBarItem8);
            this.tileBarGroup2.Items.Add(this.tileBarItem3);
            this.tileBarGroup2.Items.Add(this.tileBarItem4);
            this.tileBarGroup2.Items.Add(this.tileBarItem5);
            this.tileBarGroup2.Items.Add(this.tileBarItem6);
            this.tileBarGroup2.Name = "tileBarGroup2";
            // 
            // btnHoatDong
            // 
            this.btnHoatDong.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btnHoatDong.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnHoatDong.AppearanceItem.Normal.Options.UseBackColor = true;
            this.btnHoatDong.DropDownControl = this.tileBar_HoatDong;
            this.btnHoatDong.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement1.Image")));
            tileItemElement1.Text = "Hoạt Động";
            this.btnHoatDong.Elements.Add(tileItemElement1);
            this.btnHoatDong.Id = 0;
            this.btnHoatDong.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.btnHoatDong.Name = "btnHoatDong";
            // 
            // tileBar_HoatDong
            // 
            this.tileBar_HoatDong.Controls.Add(this.btnHoatDong_GiaoHang);
            this.tileBar_HoatDong.Controls.Add(this.btnHoatDong_TraHang);
            this.tileBar_HoatDong.Controls.Add(this.btnHoatDong_CNXuatHang);
            this.tileBar_HoatDong.Controls.Add(this.btnHoatDong_CNNhapHang);
            this.tileBar_HoatDong.Controls.Add(this.btnHoatDong_XuatHang);
            this.tileBar_HoatDong.Controls.Add(this.btnHoatDong_NhapHang);
            this.tileBar_HoatDong.Location = new System.Drawing.Point(48, 342);
            this.tileBar_HoatDong.Name = "tileBar_HoatDong";
            this.tileBar_HoatDong.Size = new System.Drawing.Size(834, 96);
            this.tileBar_HoatDong.TabIndex = 2;
            // 
            // btnHoatDong_GiaoHang
            // 
            this.btnHoatDong_GiaoHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnHoatDong_GiaoHang.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.btnHoatDong_GiaoHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnHoatDong_GiaoHang.Appearance.Options.UseBackColor = true;
            this.btnHoatDong_GiaoHang.Appearance.Options.UseFont = true;
            this.btnHoatDong_GiaoHang.Appearance.Options.UseForeColor = true;
            this.btnHoatDong_GiaoHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnHoatDong_GiaoHang.Location = new System.Drawing.Point(312, 27);
            this.btnHoatDong_GiaoHang.Name = "btnHoatDong_GiaoHang";
            this.btnHoatDong_GiaoHang.Size = new System.Drawing.Size(94, 46);
            this.btnHoatDong_GiaoHang.TabIndex = 5;
            this.btnHoatDong_GiaoHang.Text = "Giao Hàng";
            this.btnHoatDong_GiaoHang.Click += new System.EventHandler(this.btnHoatDong_GiaoHang_Click);
            // 
            // btnHoatDong_TraHang
            // 
            this.btnHoatDong_TraHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnHoatDong_TraHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnHoatDong_TraHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnHoatDong_TraHang.Appearance.Options.UseBackColor = true;
            this.btnHoatDong_TraHang.Appearance.Options.UseFont = true;
            this.btnHoatDong_TraHang.Appearance.Options.UseForeColor = true;
            this.btnHoatDong_TraHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnHoatDong_TraHang.Location = new System.Drawing.Point(212, 27);
            this.btnHoatDong_TraHang.Name = "btnHoatDong_TraHang";
            this.btnHoatDong_TraHang.Size = new System.Drawing.Size(94, 46);
            this.btnHoatDong_TraHang.TabIndex = 4;
            this.btnHoatDong_TraHang.Text = "Trả Hàng";
            // 
            // btnHoatDong_CNXuatHang
            // 
            this.btnHoatDong_CNXuatHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnHoatDong_CNXuatHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnHoatDong_CNXuatHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnHoatDong_CNXuatHang.Appearance.Options.UseBackColor = true;
            this.btnHoatDong_CNXuatHang.Appearance.Options.UseFont = true;
            this.btnHoatDong_CNXuatHang.Appearance.Options.UseForeColor = true;
            this.btnHoatDong_CNXuatHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnHoatDong_CNXuatHang.Location = new System.Drawing.Point(589, 27);
            this.btnHoatDong_CNXuatHang.Name = "btnHoatDong_CNXuatHang";
            this.btnHoatDong_CNXuatHang.Size = new System.Drawing.Size(171, 46);
            this.btnHoatDong_CNXuatHang.TabIndex = 3;
            this.btnHoatDong_CNXuatHang.Text = "Viết Công Nợ Xuất Hàng";
            // 
            // btnHoatDong_CNNhapHang
            // 
            this.btnHoatDong_CNNhapHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnHoatDong_CNNhapHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnHoatDong_CNNhapHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnHoatDong_CNNhapHang.Appearance.Options.UseBackColor = true;
            this.btnHoatDong_CNNhapHang.Appearance.Options.UseFont = true;
            this.btnHoatDong_CNNhapHang.Appearance.Options.UseForeColor = true;
            this.btnHoatDong_CNNhapHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnHoatDong_CNNhapHang.Location = new System.Drawing.Point(412, 27);
            this.btnHoatDong_CNNhapHang.Name = "btnHoatDong_CNNhapHang";
            this.btnHoatDong_CNNhapHang.Size = new System.Drawing.Size(171, 46);
            this.btnHoatDong_CNNhapHang.TabIndex = 2;
            this.btnHoatDong_CNNhapHang.Text = "Viết Công Nợ Nhập Hàng";
            // 
            // btnHoatDong_XuatHang
            // 
            this.btnHoatDong_XuatHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnHoatDong_XuatHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnHoatDong_XuatHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnHoatDong_XuatHang.Appearance.Options.UseBackColor = true;
            this.btnHoatDong_XuatHang.Appearance.Options.UseFont = true;
            this.btnHoatDong_XuatHang.Appearance.Options.UseForeColor = true;
            this.btnHoatDong_XuatHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnHoatDong_XuatHang.Location = new System.Drawing.Point(112, 27);
            this.btnHoatDong_XuatHang.Name = "btnHoatDong_XuatHang";
            this.btnHoatDong_XuatHang.Size = new System.Drawing.Size(94, 46);
            this.btnHoatDong_XuatHang.TabIndex = 1;
            this.btnHoatDong_XuatHang.Text = "Xuất Hàng";
            // 
            // btnHoatDong_NhapHang
            // 
            this.btnHoatDong_NhapHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnHoatDong_NhapHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnHoatDong_NhapHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnHoatDong_NhapHang.Appearance.Options.UseBackColor = true;
            this.btnHoatDong_NhapHang.Appearance.Options.UseFont = true;
            this.btnHoatDong_NhapHang.Appearance.Options.UseForeColor = true;
            this.btnHoatDong_NhapHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnHoatDong_NhapHang.Location = new System.Drawing.Point(12, 27);
            this.btnHoatDong_NhapHang.Name = "btnHoatDong_NhapHang";
            this.btnHoatDong_NhapHang.Size = new System.Drawing.Size(94, 46);
            this.btnHoatDong_NhapHang.TabIndex = 0;
            this.btnHoatDong_NhapHang.Text = "Nhập Hàng";
            this.btnHoatDong_NhapHang.Click += new System.EventHandler(this.btnHoatDong_NhapHang_Click);
            // 
            // tileBarItem2
            // 
            this.tileBarItem2.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.tileBarItem2.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileBarItem2.DropDownControl = this.tileBar_DanhMuc;
            this.tileBarItem2.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Image = global::ProjectExotic.Properties.Resources.search_32;
            tileItemElement2.Text = "Danh Mục";
            this.tileBarItem2.Elements.Add(tileItemElement2);
            this.tileBarItem2.Id = 1;
            this.tileBarItem2.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.tileBarItem2.Name = "tileBarItem2";
            // 
            // tileBar_DanhMuc
            // 
            this.tileBar_DanhMuc.Controls.Add(this.btnDanhMuc_ThongTinGiaoHang);
            this.tileBar_DanhMuc.Controls.Add(this.btnDanhMuc_KhachHang);
            this.tileBar_DanhMuc.Controls.Add(this.btnDanhMuc_HoaDonXuatHang);
            this.tileBar_DanhMuc.Controls.Add(this.btnDanhMuc_HoaDonNhapHang);
            this.tileBar_DanhMuc.Controls.Add(this.btnDanhMuc_Size);
            this.tileBar_DanhMuc.Controls.Add(this.btnDanhMuc_MatHang);
            this.tileBar_DanhMuc.Location = new System.Drawing.Point(32, 122);
            this.tileBar_DanhMuc.Name = "tileBar_DanhMuc";
            this.tileBar_DanhMuc.Size = new System.Drawing.Size(834, 96);
            this.tileBar_DanhMuc.TabIndex = 7;
            // 
            // btnDanhMuc_ThongTinGiaoHang
            // 
            this.btnDanhMuc_ThongTinGiaoHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.btnDanhMuc_ThongTinGiaoHang.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.btnDanhMuc_ThongTinGiaoHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDanhMuc_ThongTinGiaoHang.Appearance.Options.UseBackColor = true;
            this.btnDanhMuc_ThongTinGiaoHang.Appearance.Options.UseFont = true;
            this.btnDanhMuc_ThongTinGiaoHang.Appearance.Options.UseForeColor = true;
            this.btnDanhMuc_ThongTinGiaoHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnDanhMuc_ThongTinGiaoHang.Location = new System.Drawing.Point(312, 26);
            this.btnDanhMuc_ThongTinGiaoHang.Name = "btnDanhMuc_ThongTinGiaoHang";
            this.btnDanhMuc_ThongTinGiaoHang.Size = new System.Drawing.Size(154, 46);
            this.btnDanhMuc_ThongTinGiaoHang.TabIndex = 5;
            this.btnDanhMuc_ThongTinGiaoHang.Text = "Thông Tin Giao Hàng";
            this.btnDanhMuc_ThongTinGiaoHang.Click += new System.EventHandler(this.btnDanhMuc_ThongTinGiaoHang_Click);
            // 
            // btnDanhMuc_KhachHang
            // 
            this.btnDanhMuc_KhachHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.btnDanhMuc_KhachHang.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.btnDanhMuc_KhachHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDanhMuc_KhachHang.Appearance.Options.UseBackColor = true;
            this.btnDanhMuc_KhachHang.Appearance.Options.UseFont = true;
            this.btnDanhMuc_KhachHang.Appearance.Options.UseForeColor = true;
            this.btnDanhMuc_KhachHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnDanhMuc_KhachHang.Location = new System.Drawing.Point(212, 26);
            this.btnDanhMuc_KhachHang.Name = "btnDanhMuc_KhachHang";
            this.btnDanhMuc_KhachHang.Size = new System.Drawing.Size(94, 46);
            this.btnDanhMuc_KhachHang.TabIndex = 4;
            this.btnDanhMuc_KhachHang.Text = "Khách Hàng";
            this.btnDanhMuc_KhachHang.Click += new System.EventHandler(this.btnDanhMuc_KhachHang_Click);
            // 
            // btnDanhMuc_HoaDonXuatHang
            // 
            this.btnDanhMuc_HoaDonXuatHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.btnDanhMuc_HoaDonXuatHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnDanhMuc_HoaDonXuatHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDanhMuc_HoaDonXuatHang.Appearance.Options.UseBackColor = true;
            this.btnDanhMuc_HoaDonXuatHang.Appearance.Options.UseFont = true;
            this.btnDanhMuc_HoaDonXuatHang.Appearance.Options.UseForeColor = true;
            this.btnDanhMuc_HoaDonXuatHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnDanhMuc_HoaDonXuatHang.Location = new System.Drawing.Point(649, 26);
            this.btnDanhMuc_HoaDonXuatHang.Name = "btnDanhMuc_HoaDonXuatHang";
            this.btnDanhMuc_HoaDonXuatHang.Size = new System.Drawing.Size(171, 46);
            this.btnDanhMuc_HoaDonXuatHang.TabIndex = 3;
            this.btnDanhMuc_HoaDonXuatHang.Text = "Hóa Đơn Xuất Hàng";
            // 
            // btnDanhMuc_HoaDonNhapHang
            // 
            this.btnDanhMuc_HoaDonNhapHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.btnDanhMuc_HoaDonNhapHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnDanhMuc_HoaDonNhapHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDanhMuc_HoaDonNhapHang.Appearance.Options.UseBackColor = true;
            this.btnDanhMuc_HoaDonNhapHang.Appearance.Options.UseFont = true;
            this.btnDanhMuc_HoaDonNhapHang.Appearance.Options.UseForeColor = true;
            this.btnDanhMuc_HoaDonNhapHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnDanhMuc_HoaDonNhapHang.Location = new System.Drawing.Point(472, 26);
            this.btnDanhMuc_HoaDonNhapHang.Name = "btnDanhMuc_HoaDonNhapHang";
            this.btnDanhMuc_HoaDonNhapHang.Size = new System.Drawing.Size(171, 46);
            this.btnDanhMuc_HoaDonNhapHang.TabIndex = 2;
            this.btnDanhMuc_HoaDonNhapHang.Text = "Hóa Đơn Nhập Hàng";
            // 
            // btnDanhMuc_Size
            // 
            this.btnDanhMuc_Size.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.btnDanhMuc_Size.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.btnDanhMuc_Size.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDanhMuc_Size.Appearance.Options.UseBackColor = true;
            this.btnDanhMuc_Size.Appearance.Options.UseFont = true;
            this.btnDanhMuc_Size.Appearance.Options.UseForeColor = true;
            this.btnDanhMuc_Size.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnDanhMuc_Size.Location = new System.Drawing.Point(112, 26);
            this.btnDanhMuc_Size.Name = "btnDanhMuc_Size";
            this.btnDanhMuc_Size.Size = new System.Drawing.Size(94, 46);
            this.btnDanhMuc_Size.TabIndex = 1;
            this.btnDanhMuc_Size.Text = "Size";
            // 
            // btnDanhMuc_MatHang
            // 
            this.btnDanhMuc_MatHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.btnDanhMuc_MatHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnDanhMuc_MatHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDanhMuc_MatHang.Appearance.Options.UseBackColor = true;
            this.btnDanhMuc_MatHang.Appearance.Options.UseFont = true;
            this.btnDanhMuc_MatHang.Appearance.Options.UseForeColor = true;
            this.btnDanhMuc_MatHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnDanhMuc_MatHang.Location = new System.Drawing.Point(12, 26);
            this.btnDanhMuc_MatHang.Name = "btnDanhMuc_MatHang";
            this.btnDanhMuc_MatHang.Size = new System.Drawing.Size(94, 46);
            this.btnDanhMuc_MatHang.TabIndex = 0;
            this.btnDanhMuc_MatHang.Text = "Mặt Hàng";
            this.btnDanhMuc_MatHang.Click += new System.EventHandler(this.btnDanhMuc_MatHang_Click);
            // 
            // tileBarItem7
            // 
            this.tileBarItem7.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tileBarItem7.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileBarItem7.DropDownControl = this.tileBar_QuanLyNhapHang;
            this.tileBarItem7.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement3.Image")));
            tileItemElement3.Text = "Quản Lý Nhập Hàng";
            this.tileBarItem7.Elements.Add(tileItemElement3);
            this.tileBarItem7.Id = 12;
            this.tileBarItem7.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.tileBarItem7.Name = "tileBarItem7";
            // 
            // tileBar_QuanLyNhapHang
            // 
            this.tileBar_QuanLyNhapHang.Controls.Add(this.btnQLNH_PhieuTraCongNo);
            this.tileBar_QuanLyNhapHang.Controls.Add(this.btnQLNH_CongNoNhapHang);
            this.tileBar_QuanLyNhapHang.Controls.Add(this.btnQLNH_HoaDonNhapHang);
            this.tileBar_QuanLyNhapHang.Location = new System.Drawing.Point(22, 249);
            this.tileBar_QuanLyNhapHang.Name = "tileBar_QuanLyNhapHang";
            this.tileBar_QuanLyNhapHang.Size = new System.Drawing.Size(834, 96);
            this.tileBar_QuanLyNhapHang.TabIndex = 10;
            // 
            // btnQLNH_PhieuTraCongNo
            // 
            this.btnQLNH_PhieuTraCongNo.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLNH_PhieuTraCongNo.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLNH_PhieuTraCongNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLNH_PhieuTraCongNo.Appearance.Options.UseBackColor = true;
            this.btnQLNH_PhieuTraCongNo.Appearance.Options.UseFont = true;
            this.btnQLNH_PhieuTraCongNo.Appearance.Options.UseForeColor = true;
            this.btnQLNH_PhieuTraCongNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLNH_PhieuTraCongNo.Location = new System.Drawing.Point(311, 25);
            this.btnQLNH_PhieuTraCongNo.Name = "btnQLNH_PhieuTraCongNo";
            this.btnQLNH_PhieuTraCongNo.Size = new System.Drawing.Size(143, 46);
            this.btnQLNH_PhieuTraCongNo.TabIndex = 8;
            this.btnQLNH_PhieuTraCongNo.Text = "Phiếu Trả Công Nợ";
            // 
            // btnQLNH_CongNoNhapHang
            // 
            this.btnQLNH_CongNoNhapHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLNH_CongNoNhapHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLNH_CongNoNhapHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLNH_CongNoNhapHang.Appearance.Options.UseBackColor = true;
            this.btnQLNH_CongNoNhapHang.Appearance.Options.UseFont = true;
            this.btnQLNH_CongNoNhapHang.Appearance.Options.UseForeColor = true;
            this.btnQLNH_CongNoNhapHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLNH_CongNoNhapHang.Location = new System.Drawing.Point(162, 25);
            this.btnQLNH_CongNoNhapHang.Name = "btnQLNH_CongNoNhapHang";
            this.btnQLNH_CongNoNhapHang.Size = new System.Drawing.Size(143, 46);
            this.btnQLNH_CongNoNhapHang.TabIndex = 7;
            this.btnQLNH_CongNoNhapHang.Text = "Công Nợ Nhập Hàng";
            // 
            // btnQLNH_HoaDonNhapHang
            // 
            this.btnQLNH_HoaDonNhapHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLNH_HoaDonNhapHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLNH_HoaDonNhapHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLNH_HoaDonNhapHang.Appearance.Options.UseBackColor = true;
            this.btnQLNH_HoaDonNhapHang.Appearance.Options.UseFont = true;
            this.btnQLNH_HoaDonNhapHang.Appearance.Options.UseForeColor = true;
            this.btnQLNH_HoaDonNhapHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLNH_HoaDonNhapHang.Location = new System.Drawing.Point(13, 25);
            this.btnQLNH_HoaDonNhapHang.Name = "btnQLNH_HoaDonNhapHang";
            this.btnQLNH_HoaDonNhapHang.Size = new System.Drawing.Size(143, 46);
            this.btnQLNH_HoaDonNhapHang.TabIndex = 6;
            this.btnQLNH_HoaDonNhapHang.Text = "Hóa Đơn Nhập Hàng";
            // 
            // tileBarItem8
            // 
            this.tileBarItem8.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tileBarItem8.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileBarItem8.DropDownControl = this.tilebar_QuanLyXuatHang;
            this.tileBarItem8.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Image = global::ProjectExotic.Properties.Resources.categorize_export_32;
            tileItemElement4.Text = "Quản Lý Xuất Hàng";
            this.tileBarItem8.Elements.Add(tileItemElement4);
            this.tileBarItem8.Id = 10;
            this.tileBarItem8.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.tileBarItem8.Name = "tileBarItem8";
            // 
            // tilebar_QuanLyXuatHang
            // 
            this.tilebar_QuanLyXuatHang.Controls.Add(this.btnQLXH_ThongTinGiaoHang);
            this.tilebar_QuanLyXuatHang.Controls.Add(this.btnQLXH_PhieuTraCongNo);
            this.tilebar_QuanLyXuatHang.Controls.Add(this.btnQLXH_CongNoXuatHang);
            this.tilebar_QuanLyXuatHang.Controls.Add(this.btnQLXH_HoaDonXuatHang);
            this.tilebar_QuanLyXuatHang.Location = new System.Drawing.Point(9, 147);
            this.tilebar_QuanLyXuatHang.Name = "tilebar_QuanLyXuatHang";
            this.tilebar_QuanLyXuatHang.Size = new System.Drawing.Size(834, 96);
            this.tilebar_QuanLyXuatHang.TabIndex = 9;
            // 
            // btnQLXH_ThongTinGiaoHang
            // 
            this.btnQLXH_ThongTinGiaoHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLXH_ThongTinGiaoHang.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.btnQLXH_ThongTinGiaoHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLXH_ThongTinGiaoHang.Appearance.Options.UseBackColor = true;
            this.btnQLXH_ThongTinGiaoHang.Appearance.Options.UseFont = true;
            this.btnQLXH_ThongTinGiaoHang.Appearance.Options.UseForeColor = true;
            this.btnQLXH_ThongTinGiaoHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLXH_ThongTinGiaoHang.Location = new System.Drawing.Point(460, 25);
            this.btnQLXH_ThongTinGiaoHang.Name = "btnQLXH_ThongTinGiaoHang";
            this.btnQLXH_ThongTinGiaoHang.Size = new System.Drawing.Size(149, 46);
            this.btnQLXH_ThongTinGiaoHang.TabIndex = 9;
            this.btnQLXH_ThongTinGiaoHang.Text = "Thông Tin Giao Hàng";
            this.btnQLXH_ThongTinGiaoHang.Click += new System.EventHandler(this.btnQLXH_ThongTinGiaoHang_Click);
            // 
            // btnQLXH_PhieuTraCongNo
            // 
            this.btnQLXH_PhieuTraCongNo.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLXH_PhieuTraCongNo.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLXH_PhieuTraCongNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLXH_PhieuTraCongNo.Appearance.Options.UseBackColor = true;
            this.btnQLXH_PhieuTraCongNo.Appearance.Options.UseFont = true;
            this.btnQLXH_PhieuTraCongNo.Appearance.Options.UseForeColor = true;
            this.btnQLXH_PhieuTraCongNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLXH_PhieuTraCongNo.Location = new System.Drawing.Point(311, 25);
            this.btnQLXH_PhieuTraCongNo.Name = "btnQLXH_PhieuTraCongNo";
            this.btnQLXH_PhieuTraCongNo.Size = new System.Drawing.Size(143, 46);
            this.btnQLXH_PhieuTraCongNo.TabIndex = 8;
            this.btnQLXH_PhieuTraCongNo.Text = "Phiếu Trả Công Nợ";
            // 
            // btnQLXH_CongNoXuatHang
            // 
            this.btnQLXH_CongNoXuatHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLXH_CongNoXuatHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLXH_CongNoXuatHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLXH_CongNoXuatHang.Appearance.Options.UseBackColor = true;
            this.btnQLXH_CongNoXuatHang.Appearance.Options.UseFont = true;
            this.btnQLXH_CongNoXuatHang.Appearance.Options.UseForeColor = true;
            this.btnQLXH_CongNoXuatHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLXH_CongNoXuatHang.Location = new System.Drawing.Point(162, 25);
            this.btnQLXH_CongNoXuatHang.Name = "btnQLXH_CongNoXuatHang";
            this.btnQLXH_CongNoXuatHang.Size = new System.Drawing.Size(143, 46);
            this.btnQLXH_CongNoXuatHang.TabIndex = 7;
            this.btnQLXH_CongNoXuatHang.Text = "Công Nợ Xuất Hàng";
            // 
            // btnQLXH_HoaDonXuatHang
            // 
            this.btnQLXH_HoaDonXuatHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLXH_HoaDonXuatHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLXH_HoaDonXuatHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLXH_HoaDonXuatHang.Appearance.Options.UseBackColor = true;
            this.btnQLXH_HoaDonXuatHang.Appearance.Options.UseFont = true;
            this.btnQLXH_HoaDonXuatHang.Appearance.Options.UseForeColor = true;
            this.btnQLXH_HoaDonXuatHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLXH_HoaDonXuatHang.Location = new System.Drawing.Point(13, 25);
            this.btnQLXH_HoaDonXuatHang.Name = "btnQLXH_HoaDonXuatHang";
            this.btnQLXH_HoaDonXuatHang.Size = new System.Drawing.Size(143, 46);
            this.btnQLXH_HoaDonXuatHang.TabIndex = 6;
            this.btnQLXH_HoaDonXuatHang.Text = "Hóa Đơn Xuất Hàng";
            // 
            // tileBarItem3
            // 
            this.tileBarItem3.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.tileBarItem3.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileBarItem3.DropDownControl = this.tileBar_QuanLyThongTin;
            this.tileBarItem3.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Image = global::ProjectExotic.Properties.Resources.categorize_32;
            tileItemElement5.Text = "Quản Lý Thông Tin";
            this.tileBarItem3.Elements.Add(tileItemElement5);
            this.tileBarItem3.Id = 2;
            this.tileBarItem3.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.tileBarItem3.Name = "tileBarItem3";
            // 
            // tileBar_QuanLyThongTin
            // 
            this.tileBar_QuanLyThongTin.Controls.Add(this.btnQLTT_NhanVien);
            this.tileBar_QuanLyThongTin.Controls.Add(this.btnQLTT_KhachHang);
            this.tileBar_QuanLyThongTin.Controls.Add(this.btnQLTT_Size);
            this.tileBar_QuanLyThongTin.Controls.Add(this.btnQLTT_MatHang);
            this.tileBar_QuanLyThongTin.Controls.Add(this.btnQLTT_ThuongHieu);
            this.tileBar_QuanLyThongTin.Controls.Add(this.btnQLTT_NhaCungCap);
            this.tileBar_QuanLyThongTin.Location = new System.Drawing.Point(61, 20);
            this.tileBar_QuanLyThongTin.Name = "tileBar_QuanLyThongTin";
            this.tileBar_QuanLyThongTin.Size = new System.Drawing.Size(923, 96);
            this.tileBar_QuanLyThongTin.TabIndex = 12;
            // 
            // btnQLTT_NhanVien
            // 
            this.btnQLTT_NhanVien.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLTT_NhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLTT_NhanVien.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLTT_NhanVien.Appearance.Options.UseBackColor = true;
            this.btnQLTT_NhanVien.Appearance.Options.UseFont = true;
            this.btnQLTT_NhanVien.Appearance.Options.UseForeColor = true;
            this.btnQLTT_NhanVien.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLTT_NhanVien.Location = new System.Drawing.Point(750, 25);
            this.btnQLTT_NhanVien.Name = "btnQLTT_NhanVien";
            this.btnQLTT_NhanVien.Size = new System.Drawing.Size(107, 46);
            this.btnQLTT_NhanVien.TabIndex = 12;
            this.btnQLTT_NhanVien.Text = "Nhân Viên";
            this.btnQLTT_NhanVien.Click += new System.EventHandler(this.btnQLTT_NhanVien_Click);
            // 
            // btnQLTT_KhachHang
            // 
            this.btnQLTT_KhachHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLTT_KhachHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLTT_KhachHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLTT_KhachHang.Appearance.Options.UseBackColor = true;
            this.btnQLTT_KhachHang.Appearance.Options.UseFont = true;
            this.btnQLTT_KhachHang.Appearance.Options.UseForeColor = true;
            this.btnQLTT_KhachHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLTT_KhachHang.Location = new System.Drawing.Point(637, 25);
            this.btnQLTT_KhachHang.Name = "btnQLTT_KhachHang";
            this.btnQLTT_KhachHang.Size = new System.Drawing.Size(107, 46);
            this.btnQLTT_KhachHang.TabIndex = 11;
            this.btnQLTT_KhachHang.Text = "Khách Hàng";
            this.btnQLTT_KhachHang.Click += new System.EventHandler(this.btnQLTT_KhachHang_Click);
            // 
            // btnQLTT_Size
            // 
            this.btnQLTT_Size.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLTT_Size.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLTT_Size.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLTT_Size.Appearance.Options.UseBackColor = true;
            this.btnQLTT_Size.Appearance.Options.UseFont = true;
            this.btnQLTT_Size.Appearance.Options.UseForeColor = true;
            this.btnQLTT_Size.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLTT_Size.Location = new System.Drawing.Point(507, 25);
            this.btnQLTT_Size.Name = "btnQLTT_Size";
            this.btnQLTT_Size.Size = new System.Drawing.Size(107, 46);
            this.btnQLTT_Size.TabIndex = 10;
            this.btnQLTT_Size.Text = "Size";
            // 
            // btnQLTT_MatHang
            // 
            this.btnQLTT_MatHang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLTT_MatHang.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLTT_MatHang.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLTT_MatHang.Appearance.Options.UseBackColor = true;
            this.btnQLTT_MatHang.Appearance.Options.UseFont = true;
            this.btnQLTT_MatHang.Appearance.Options.UseForeColor = true;
            this.btnQLTT_MatHang.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLTT_MatHang.Location = new System.Drawing.Point(369, 25);
            this.btnQLTT_MatHang.Name = "btnQLTT_MatHang";
            this.btnQLTT_MatHang.Size = new System.Drawing.Size(132, 46);
            this.btnQLTT_MatHang.TabIndex = 9;
            this.btnQLTT_MatHang.Text = "Mặt Hàng";
            this.btnQLTT_MatHang.Click += new System.EventHandler(this.btnQLTT_MatHang_Click);
            // 
            // btnQLTT_ThuongHieu
            // 
            this.btnQLTT_ThuongHieu.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLTT_ThuongHieu.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLTT_ThuongHieu.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLTT_ThuongHieu.Appearance.Options.UseBackColor = true;
            this.btnQLTT_ThuongHieu.Appearance.Options.UseFont = true;
            this.btnQLTT_ThuongHieu.Appearance.Options.UseForeColor = true;
            this.btnQLTT_ThuongHieu.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLTT_ThuongHieu.Location = new System.Drawing.Point(135, 25);
            this.btnQLTT_ThuongHieu.Name = "btnQLTT_ThuongHieu";
            this.btnQLTT_ThuongHieu.Size = new System.Drawing.Size(107, 46);
            this.btnQLTT_ThuongHieu.TabIndex = 7;
            this.btnQLTT_ThuongHieu.Text = "Thương Hiệu";
            this.btnQLTT_ThuongHieu.Click += new System.EventHandler(this.btnQLTT_ThuongHieu_Click);
            // 
            // btnQLTT_NhaCungCap
            // 
            this.btnQLTT_NhaCungCap.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.btnQLTT_NhaCungCap.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnQLTT_NhaCungCap.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnQLTT_NhaCungCap.Appearance.Options.UseBackColor = true;
            this.btnQLTT_NhaCungCap.Appearance.Options.UseFont = true;
            this.btnQLTT_NhaCungCap.Appearance.Options.UseForeColor = true;
            this.btnQLTT_NhaCungCap.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnQLTT_NhaCungCap.Location = new System.Drawing.Point(10, 25);
            this.btnQLTT_NhaCungCap.Name = "btnQLTT_NhaCungCap";
            this.btnQLTT_NhaCungCap.Size = new System.Drawing.Size(107, 46);
            this.btnQLTT_NhaCungCap.TabIndex = 6;
            this.btnQLTT_NhaCungCap.Text = "Nhà Cung Cấp";
            // 
            // tileBarItem4
            // 
            this.tileBarItem4.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(67)))), ((int)(((byte)(21)))));
            this.tileBarItem4.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileBarItem4.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Image = global::ProjectExotic.Properties.Resources.pie_chart_32;
            tileItemElement6.Text = "Thống Kê";
            this.tileBarItem4.Elements.Add(tileItemElement6);
            this.tileBarItem4.Id = 3;
            this.tileBarItem4.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.tileBarItem4.Name = "tileBarItem4";
            // 
            // tileBarItem5
            // 
            this.tileBarItem5.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(175)))), ((int)(((byte)(80)))));
            this.tileBarItem5.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileBarItem5.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Image = global::ProjectExotic.Properties.Resources.frame_32;
            tileItemElement7.Text = "Hệ Thống";
            this.tileBarItem5.Elements.Add(tileItemElement7);
            this.tileBarItem5.Id = 4;
            this.tileBarItem5.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.tileBarItem5.Name = "tileBarItem5";
            // 
            // tileBarItem6
            // 
            this.tileBarItem6.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.tileBarItem6.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileBarItem6.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Image = global::ProjectExotic.Properties.Resources.help_32;
            tileItemElement8.Text = "Hỗ trợ";
            this.tileBarItem6.Elements.Add(tileItemElement8);
            this.tileBarItem6.Id = 8;
            this.tileBarItem6.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.tileBarItem6.Name = "tileBarItem6";
            // 
            // tileBarItem9
            // 
            this.tileBarItem9.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "tileBarItem9";
            this.tileBarItem9.Elements.Add(tileItemElement9);
            this.tileBarItem9.Id = 2;
            this.tileBarItem9.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Medium;
            this.tileBarItem9.Name = "tileBarItem9";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Name = "tileGroup3";
            // 
            // panelFooter
            // 
            this.panelFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.panelFooter.Controls.Add(this.panel1);
            this.panelFooter.Controls.Add(this.btnDangXuat);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 471);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(996, 26);
            this.panelFooter.TabIndex = 18;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(387, 26);
            this.panel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nhân Viên Đăng Nhập : ";
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnDangXuat.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnDangXuat.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnDangXuat.Appearance.Options.UseBackColor = true;
            this.btnDangXuat.Appearance.Options.UseFont = true;
            this.btnDangXuat.Appearance.Options.UseForeColor = true;
            this.btnDangXuat.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnDangXuat.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDangXuat.Image = ((System.Drawing.Image)(resources.GetObject("btnDangXuat.Image")));
            this.btnDangXuat.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnDangXuat.Location = new System.Drawing.Point(856, 0);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(140, 26);
            this.btnDangXuat.TabIndex = 1;
            this.btnDangXuat.Text = "Đăng Xuất";
            this.btnDangXuat.Click += new System.EventHandler(this.btnDangXuat_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabControl1.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabControl1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 101);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.Size = new System.Drawing.Size(996, 370);
            this.xtraTabControl1.TabIndex = 19;
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.CloseButtonClick += new System.EventHandler(this.xtraTabControl1_CloseButtonClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 497);
            this.Controls.Add(this.tileBar_DanhMuc);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.tileBar_QuanLyThongTin);
            this.Controls.Add(this.tileBar_QuanLyNhapHang);
            this.Controls.Add(this.tilebar_QuanLyXuatHang);
            this.Controls.Add(this.tileBar1);
            this.Controls.Add(this.tileBar_HoatDong);
            this.LookAndFeel.SkinName = "Office 2013";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phần mềm quản lý kinh doanh thời trang iFashion version 2.0";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tileBar_HoatDong)).EndInit();
            this.tileBar_HoatDong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tileBar_DanhMuc)).EndInit();
            this.tileBar_DanhMuc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tileBar_QuanLyNhapHang)).EndInit();
            this.tileBar_QuanLyNhapHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tilebar_QuanLyXuatHang)).EndInit();
            this.tilebar_QuanLyXuatHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tileBar_QuanLyThongTin)).EndInit();
            this.tileBar_QuanLyThongTin.ResumeLayout(false);
            this.panelFooter.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileBarGroup tileBarGroup7;
        private DevExpress.XtraBars.Navigation.TileBar tileBar1;
        private DevExpress.XtraBars.Navigation.TileBarGroup tileBarGroup2;
        private DevExpress.XtraBars.Navigation.TileBarItem btnHoatDong;
        private DevExpress.XtraBars.Navigation.TileBarItem tileBarItem2;
        private DevExpress.XtraBars.Navigation.TileBarItem tileBarItem3;
        private DevExpress.XtraBars.Navigation.TileBarItem tileBarItem4;
        private DevExpress.XtraBars.Navigation.TileBarItem tileBarItem5;
        private DevExpress.XtraBars.Navigation.TileBarItem tileBarItem6;
        private DevExpress.XtraBars.Navigation.TileBarItem tileBarItem9;
        private DevExpress.XtraBars.Navigation.TileBarItem tileBarItem8;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraBars.Navigation.TileBarItem tileBarItem7;
        private System.Windows.Forms.Panel panelFooter;
        private DevExpress.XtraEditors.SimpleButton btnDangXuat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraBars.Navigation.TileBarDropDownContainer tileBar_HoatDong;
        private DevExpress.XtraEditors.SimpleButton btnHoatDong_GiaoHang;
        private DevExpress.XtraEditors.SimpleButton btnHoatDong_TraHang;
        private DevExpress.XtraEditors.SimpleButton btnHoatDong_CNXuatHang;
        private DevExpress.XtraEditors.SimpleButton btnHoatDong_CNNhapHang;
        private DevExpress.XtraEditors.SimpleButton btnHoatDong_XuatHang;
        private DevExpress.XtraEditors.SimpleButton btnHoatDong_NhapHang;
        private DevExpress.XtraBars.Navigation.TileBarDropDownContainer tileBar_DanhMuc;
        private DevExpress.XtraEditors.SimpleButton btnDanhMuc_ThongTinGiaoHang;
        private DevExpress.XtraEditors.SimpleButton btnDanhMuc_KhachHang;
        private DevExpress.XtraEditors.SimpleButton btnDanhMuc_HoaDonXuatHang;
        private DevExpress.XtraEditors.SimpleButton btnDanhMuc_HoaDonNhapHang;
        private DevExpress.XtraEditors.SimpleButton btnDanhMuc_Size;
        private DevExpress.XtraEditors.SimpleButton btnDanhMuc_MatHang;
        private DevExpress.XtraBars.Navigation.TileBarDropDownContainer tileBar_QuanLyNhapHang;
        private DevExpress.XtraEditors.SimpleButton btnQLNH_PhieuTraCongNo;
        private DevExpress.XtraEditors.SimpleButton btnQLNH_CongNoNhapHang;
        private DevExpress.XtraEditors.SimpleButton btnQLNH_HoaDonNhapHang;
        private DevExpress.XtraBars.Navigation.TileBarDropDownContainer tilebar_QuanLyXuatHang;
        private DevExpress.XtraEditors.SimpleButton btnQLXH_ThongTinGiaoHang;
        private DevExpress.XtraEditors.SimpleButton btnQLXH_PhieuTraCongNo;
        private DevExpress.XtraEditors.SimpleButton btnQLXH_CongNoXuatHang;
        private DevExpress.XtraEditors.SimpleButton btnQLXH_HoaDonXuatHang;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraBars.Navigation.TileBarDropDownContainer tileBar_QuanLyThongTin;
        private DevExpress.XtraEditors.SimpleButton btnQLTT_NhanVien;
        private DevExpress.XtraEditors.SimpleButton btnQLTT_KhachHang;
        private DevExpress.XtraEditors.SimpleButton btnQLTT_Size;
        private DevExpress.XtraEditors.SimpleButton btnQLTT_MatHang;
        private DevExpress.XtraEditors.SimpleButton btnQLTT_ThuongHieu;
        private DevExpress.XtraEditors.SimpleButton btnQLTT_NhaCungCap;



    }
}