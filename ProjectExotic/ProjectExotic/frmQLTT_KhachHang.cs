﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExoticDTO;
using ExoticDAL;
using ExoticBLL;

namespace ProjectExotic
{
    public partial class frmQLTT_KhachHang : DevExpress.XtraEditors.XtraForm
    {
        ShopCommerceDataContext dbs = null;
        #region các biến-Hàm
        // dữ liệu khách hàng
        bllCustomers bll=null;
        dtoCustomers dto=null;
        public static string customerID;// biến dùng khi cập nhật khách hàng
        // dữ liệu loại khách hàng
        bllCustomerTypes bllCusType = null;
        dtoCustomerTypes dtoCusType = null;
        public static string customerTypeID;// biến dùng khi cập nhật loại khách hàng
        public static string customerTypeName;
        public static bool them;// biến nhận biết là đang thêm hay cập nhật dữ liệu
                                // true là button thêm
                                // false là button update

        // load dữ liệu
        void loadData()
        {
            bll = new bllCustomers();
            gridControlQLTT_KhachHang.DataSource = bll.getTable();
            bllCusType = new bllCustomerTypes();
            gridControl1.DataSource = bllCusType.getTable();
        }
        // binding dữ liệu lên thông tin khách hàng
        void bindingData()
        {
            try
            {
                lblCustomerID.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["CustomerID"]).ToString();
                lblCustomerType.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["CustomerTypeName"]).ToString();
                lblFirstName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["FirstName"]).ToString();
                lblLastName.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["LastName"]).ToString();
                lblIDNumber.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["IDNumber"]).ToString();
                lblRegisterDate.Text = Convert.ToDateTime(gridView1.GetFocusedRowCellValue(gridView1.Columns["RegisterDate"])).ToString("dd/MM/yyyy");
                lblAddress.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Address"]).ToString();
                lblPhone.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone"]).ToString();
                // neu truong hop phone 1 null
                if (gridView1.Columns["Phone1"] == null)
                {
                    lblPhone1.Text = "";
                }
                else
                {
                    lblPhone1.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Phone1"]).ToString();
                }
                lblEmail.Text = gridView1.GetFocusedRowCellValue(gridView1.Columns["Email"]).ToString();
                lblGender.Text = Convert.ToBoolean(gridView1.GetFocusedRowCellValue(gridView1.Columns["Gender"])) == true ? "Nữ" : "Nam";

            }
            //tránh xảy ra lỗi khi 1 ô dữ liệu bằng null
            catch (NullReferenceException)
            {

            }
            catch (InvalidCastException)
            { }
        }
        #endregion
        // focusInARow dùng để sau khi ta cập nhật hay thêm 1 khách hàng mới sẽ trỏ thẳng đến thằng mới luôn
        void focusInARow(string var)
        {

            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (var == gridView1.GetRowCellValue(i, gridView1.Columns["CustomerID"]).ToString())
                {
                    gridView1.FocusedRowHandle = i;
                    break;
                }
            }
        }
        #region Thêm sửa xóa khách hàng
        // thêm dữ liệu khách hàng
        void addDataCustomer()
        {
            them = true;
            frmThemCapNhat_KH frm = new frmThemCapNhat_KH();
            frm.Text = "Thêm Khách Hàng";
            frm.ShowDialog();
            frmQLTT_KhachHang_Load(null, null);
            focusInARow(frm.customerID);
        }
        void updateDataCustomer()
        {
            them = false;
            if (gridView1.SelectedRowsCount == 0)
            {
                MessageBox.Show("Bạn phải chọn một khách hàng để cập nhật", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                frmThemCapNhat_KH frm = new frmThemCapNhat_KH();
                customerID = lblCustomerID.Text;
                frm.Text = "Cập Nhật Khách Hàng";
                frm.ShowDialog();
                frmQLTT_KhachHang_Load(null, null);
                focusInARow(frm.customerID);

            }
        }

        // xóa dữ liệu khách hàng
        void deleteDataCustomer()
        {
            if (gridView1.SelectedRowsCount == 0)
            {
                MessageBox.Show("Bạn phải chọn một khách hàng để xóa", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dto = new dtoCustomers();
                bll = new bllCustomers();
                int r = gridView1.FocusedRowHandle;
                dto.CustomerID = gridView1.GetRowCellValue(r, gridView1.Columns["CustomerID"]).ToString();
                DialogResult dialog = MessageBox.Show("Bạn thật sự muốn xóa khách hàng này không?", "Cảnh báo",
                   MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (dialog == DialogResult.OK)
                {
                    string err = "";
                    bool f = bll.deleteData(dto, ref err);
                    if (!f)
                    {
                        MessageBox.Show("Xóa không thành công. Lỗi : " + err, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    frmQLTT_KhachHang_Load(null, null);
                }
            }
        }
        #endregion 

        #region Thêm sửa xóa Loại KH
        // thêm dữ liệu loại khách hàng
        void addDataCustomerType()
        {
            them = true;
            frmThemCapNhat_LoaiKH frm = new frmThemCapNhat_LoaiKH();
            frm.Text = "Thêm Loại Khách Hàng";
            frm.ShowDialog();
            frmQLTT_KhachHang_Load(null, null);
        }
        
        // cập nhật dữ liệu loại khách hàng
        void updateDataCustomerType()
        {
            them = false;
            if(gridView2.SelectedRowsCount==00)
            {
                MessageBox.Show("Bạn phải chọn một hàng để cập nhật", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                frmThemCapNhat_LoaiKH frm = new frmThemCapNhat_LoaiKH();
                frm.Text = "Cập Nhật Loại Khách Hàng";
                customerTypeID = gridView2.GetFocusedRowCellValue(gridView2.Columns["CustomerTypeID"]).ToString();
                frm.ShowDialog();
                frmQLTT_KhachHang_Load(null, null);
            }
        }

        // xóa dữ liệu loại khách hàng
        void deleteDataCustomerType()
        {
            if(gridView2.SelectedRowsCount==0)
            {
                MessageBox.Show("Bạn phải chọn một hàng để xóa", "Lỗi",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dtoCusType = new dtoCustomerTypes();
                bllCusType = new bllCustomerTypes();
                int r = gridView2.FocusedRowHandle;
                dtoCusType.CustomerTypeID = gridView2.GetRowCellValue(r, gridView2.Columns["CustomerTypeID"]).ToString();
                DialogResult dialog = MessageBox.Show("Bạn thật sự muốn xóa khách hàng này không?", "Cảnh báo",
                   MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (dialog == DialogResult.OK)
                {
                    string err = "";
                    bool f = bllCusType.deleteData(dtoCusType,ref err);
                    if (!f)
                    {
                        MessageBox.Show("Xóa không thành công. Lỗi : " + err, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    frmQLTT_KhachHang_Load(null, null);

                }
            }
        }
        #endregion


        //  các sự kiện
        #region các sự kiện
        public frmQLTT_KhachHang()
        {
            InitializeComponent();
        }

        private void frmQLTT_KhachHang_Load(object sender, EventArgs e)
        {
            loadData();
            gridView1_Click(null, null);

        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            bindingData();
        }

        private void btnAddCus_Click(object sender, EventArgs e)
        {
            addDataCustomer();
        }

        private void btnUpdateCus_Click(object sender, EventArgs e)
        {
            updateDataCustomer();
        }

        private void btnDelCus_Click(object sender, EventArgs e)
        {
            deleteDataCustomer();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            frmQLTT_KhachHang_Load(null, null);
        }

        private void btnAddCusType_Click(object sender, EventArgs e)
        {
            addDataCustomerType();
        }

        private void btnUpdateCusType_Click(object sender, EventArgs e)
        {
            updateDataCustomerType();
        }

        private void btnDelCusType_Click(object sender, EventArgs e)
        {
            deleteDataCustomerType();
        }

        private void gridView2_Click(object sender, EventArgs e)
        {
            try
            {
                dbs = new ShopCommerceDataContext();
                // dòng đang được trỏ vào
                int r = gridView2.FocusedRowHandle;
                // click vào loại khách hàng nào thì gridview kh sẽ hiển thị loại kh đó
                string CutomerTypeID = gridView2.GetRowCellValue(r, gridView2.Columns["CustomerTypeID"]).ToString();
                var cus = from n in dbs.Customers
                          where n.CustomerTypeID == CutomerTypeID
                          select n;
                gridControlQLTT_KhachHang.DataSource = cus;
            }
            catch (NullReferenceException)
            { }
        }


    }
}
        #endregion